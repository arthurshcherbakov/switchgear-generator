﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testFacotConect.Model
{
    //цепь между двумя элементами в пнимании матрицы R
    public class Chain
    {
        public int Duration = 0;
        public int CountConnection = 0;
        public List<Element> Elements;

        public Chain()
        {
            Elements = new List<Element>();
        }
    }
}
