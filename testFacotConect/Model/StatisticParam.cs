﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testFacotConect.Model
{
    // переделать так PropertyScheme простохранил развёрнутое описание схемы 

    // создать ещё класс СТАТИСТИК, который будет входить в PropertyScheme и будет содержать в себе гистограммы
    // вопрос - какая должна быть архитектура этого класаа что бы он подошёл для анализа и генерации 
    // вопрос где и каким образом должны быть распределены алгоритмы генерации

    // ответ анализ часть будет содержаться в классе, а генератор част будет в самом классе 

    //гистограмма как отдельный обькт со своими свойствами и параметрами
    //ярковыроженность гистограммы

    //класс в котором храниться гистограмма и параметры применимые к ней
    public class StatisticParam
    {
        //гистограмма 
        //int[elName , count_elName] 
        public List<int[]> Histogram;

        //максимальное минимальнео значение в выборке
        public int minValue;
        public int maxValue;

        //размах выборки
        public int rangeValue;

        //средние значение в выборке
        public double mediumValue;        

        //мода выборки 
        public int modeValue{get{return maxValue;}}

        //медиана выборки 
        public int medianValue;

        //дисперсия выборки
        public double dispersionValue;

        //стандартное отклонение выборки | Среднеквадратическое отклонение
        public double DSValue;

        //квантили выборки
        public int leftKvantilValue;
        public int rightKvantilValue;

        //меж квартельный размах
        public int rangeKvantilValue;

        //Z шкала. каждая целая единица зовётся сигмой
        List<double> Z_indexValue;

        //стандартная ошибка среднего
        public double SEValue;

        //максимальное минимальнео значение количество элементов в столбце
        public int columnMaxValue;
        public int columnMinValue;

        //максимальная разность между соседними столбцами       
        public int columnMaxValueDifference;

        //минимальная разность между соседними столбцами
        public int columnMinValueDifference;

        //средние значение элементов элементов в столбце
        public double columnMediumValue;

        //средние значение разности соседних элементов элементов в столбце
        public double columnMediumDifferenceValue;

        //лист разностей между соседними колонками
        public List<int> columnDifferenceValue;
       
        public StatisticParam()
        {
            Histogram = new List<int[]>();
            columnDifferenceValue = new List<int>();
        }

        
        public StatisticParam(List<int> elements) 
        {
            Histogram = new List<int[]>();
            columnDifferenceValue = new List<int>();
            Z_indexValue = new List<double>();
            mediumValue = 0;

            maxValue = elements.Max();
            minValue = elements.Min();

            for (int i = minValue; i <= maxValue; i++)
            {
                Histogram.Add(new int[] { i, 0 });
            }

            foreach (var el in elements)
            {
                Histogram[el - minValue][1]++;
            }

            for (int i = 0; i < elements.Count(); i++)
            {
                mediumValue += elements[i];
            }
            mediumValue /= elements.Count();

            int medianIndex = elements.Count() + 1 % 2 == 0 ? Convert.ToInt16(elements.Count() / 2) - 1 : Convert.ToInt16(elements.Count() / 2);
            medianValue = elements[medianIndex];

            rangeValue = maxValue - minValue;

            foreach (var el in elements)
            {
                dispersionValue += ((el - mediumValue) * (el - mediumValue));
            }
            dispersionValue /= elements.Count();
            DSValue = Math.Sqrt(dispersionValue);

            leftKvantilValue = medianIndex + 1 % 2 == 0 ? Convert.ToInt16(medianIndex / 2) - 1 : Convert.ToInt16(medianIndex / 2);
            rightKvantilValue = leftKvantilValue + medianIndex + 1;

            rangeKvantilValue = rightKvantilValue - leftKvantilValue;

            foreach (var el in Histogram)
            {
                Z_indexValue.Add((el[0] - mediumValue) / DSValue);
            }

            SEValue = DSValue / Math.Sqrt(elements.Count());


            calcValueColumn();            
        }

        public void calcValueColumn()
        {
            columnMediumDifferenceValue = 0;
            columnMediumValue = 0;
            for (int i = 0; i < Histogram.Count() - 1; i++)
            {
                columnDifferenceValue.Add(Math.Abs(Histogram[i][1] - Histogram[i + 1][1]));
                columnMediumDifferenceValue += columnDifferenceValue.Last();
                columnMediumValue += Histogram[i][1];
            }

            columnMediumDifferenceValue /= columnDifferenceValue.Count();
            columnMediumValue /= columnDifferenceValue.Count();
            
            columnMaxValue = Histogram.Max(x => x[1]);
            columnMaxValue = Histogram.Min(x => x[1]);

            columnMaxValueDifference = columnDifferenceValue.Max();
            columnMinValueDifference = columnDifferenceValue.Min();
        }
    }
}
