﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testFacotConect.Model
{
    public class RangeItem
    {
        public List<Element> Elements;
        public List<Chain> Chains;

        public int LeftRange;
        public int RightRange;
        

        //сколько ещё можно добавить в диапозон
        public int AddFCDouble;
        public int AddCC;
        
        int _goalFC;
        public int _goalCC;

        // цель набрать ФС для этого диапозона вычисляется из стартовой суммы и добавчного фс 
        public int GoalFC 
        {
            get { return _goalFC; }
            set 
            { 
                _goalFC = value;
                AddFCDouble = value;
            }
        }

        public int GoalCC
        {
            get { return _goalFC; }
            set
            {
                _goalCC = value;
                AddCC = value;
            }
        }

        public override string ToString()
        {
            return "L:" + Convert.ToString(LeftRange) + " R:" + Convert.ToString(RightRange) + " add: " + Convert.ToString(AddFCDouble);
        }


        //функция получения ФС тоесть количество Chains
        //получение внутренних и внешних ФС

        public RangeItem() 
        {
            Elements = new List<Element>();
            Chains = new List<Chain>();
        }

    }
}
