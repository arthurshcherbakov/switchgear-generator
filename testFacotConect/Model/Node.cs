﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testFacotConect.Model
{
    //узел в который входит несколько элементов
    public class Node
    {
        public List<Node> Nodes;
        public List<Element> Elements;

        //общие элементы текущей ноды с нодой соответствующему индексу из Nodes
        public List<Element>[] CommonElementsNodes;
        public int CountElements { get { return Elements.Count(); } }
        public int CountConnetionNodes { get { return Nodes.Count(); } }

        //получить количество общих элементов с нодой
        public int CountCommonElementsNode(Node node)
        {
            return CommonElementsNodes[Nodes.IndexOf(node)].Count();
        }
        public void GenerateBit()
        {
            CommonElementsNodes = new List<Element>[Nodes.Count()];
            for (int i = 0; i < Nodes.Count(); i++)
            {
                CommonElementsNodes[i] = new List<Element>();
            }
        }

        public Node()
        {
            Elements = new List<Element>();
            Nodes = new List<Node>();

        }
    }
}
