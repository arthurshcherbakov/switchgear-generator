﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testFacotConect.Model
{
    public class PropertyScheme
    {
        public List<Element> Elements;
        public List<Node> Nodes;
        public List<Chain> Chains;

        public StatisticParam FactConnetionStat;
        public StatisticParam CountConnetionStat;

        public List<int> listOFDeferenceFC;
        public List<int> listOFDeferenceCC;

        // гистограмма всех вариантов из HistogramCountFactConnetion(A) и HistogramCountConnetion(b). 
        //int[index-A, index-B, count]
        //(количество фактов связи, количество связи, количество)
        public List<int[]> HistogramCountAndFactConnetion;

        //отношение количества связей к фактам связей        
        public double CConFC;

        public double MaxFactConnetion;

        public int MaxCountConnetion;
        public int MinCountConnetion;

        public int CountFactConnetion;
        public double CountFactConnetionPercent;

        public int CountElements { get { return Elements.Count(); } }

        //матрица имитирующая схему КП
        public Element[][] PlacementElements;
        public PropertyScheme()
        {
            Elements = new List<Element>();
            Nodes = new List<Node>();
            Chains = new List<Chain>();

            listOFDeferenceFC = new List<int>();
            listOFDeferenceCC = new List<int>();
        }

        //добавить остальные параметры
    }
}
