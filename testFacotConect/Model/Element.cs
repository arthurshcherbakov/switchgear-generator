﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testFacotConect.Model
{
    public class Element
    {
        public string Name;
        public List<Element> Elements;
        public List<Node> Nodes;
        public List<Chain> Chains;
        public RangeItem RangeItem;

        //тестовые ништяки
        public string code;

        public int FactConnection { get { return Elements.Count(); } }


        public int CountConnection;
        //деление: CountConnection / FactConnection
        public double CConFC;

        //не зделанно
        //количество пинов элемента в соответствующем узле
        public List<int> CountConnectNode;

        public Element()
        {
            Elements = new List<Element>();
            Nodes = new List<Node>();
            Chains = new List<Chain>();
            CountConnectNode = new List<int>();
        }

        public bool HaveFC(Element el)
        {
            return Elements.IndexOf(el) == -1 ? false : true;
        }
        

        //вычисление количество связей
        public void CalculationCountConnection()
        {
            CountConnection = 0;

            foreach (var item in Chains)
            {
                CountConnection += item.CountConnection;
            }
        }

        /// <summary>
        /// Принимает ссылку на элемент с которым нужно найти количество связей. Возвращает количество связей.  
        /// </summary>        
        public int CCElement(Element el)
        {
            return Chains[Elements.IndexOf(el)].CountConnection;
        }

        public override string ToString()
        {
            return "C:" + Convert.ToString(Chains.Count) ;
        }
    }
}


