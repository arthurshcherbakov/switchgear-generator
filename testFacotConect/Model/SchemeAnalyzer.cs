﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testFacotConect.Model
{
    public class SchemeAnalyzer
    {
        public PropertyScheme Cheme;
        //порядок анализа 
        //1 создание Elements и именование их
        //2 заполение Chains и поиск связи между Chains и Elements
        //3 поиск количества связей у всех элементов
        //4 поиск связей между узлами и элементами
        //5 поиск всех связей нодов между собой
        //6 поиск общих элементов между нодами

        public SchemeAnalyzer(FileReader cheme)
        {
            Cheme = new PropertyScheme();

            Cheme.CountFactConnetion = 0;
            //Сheme.HistogramCountFactConnetion = new List<int[]>();
            //Сheme.HistogramCountConnetion = new List<int[]>();
            Cheme.HistogramCountAndFactConnetion = new List<int[]>();

            for (int i = 0; i < cheme.Elements.Count(); i++)
            {
                Cheme.Elements.Add(new Element() { Name = cheme.Elements[i] });
            }
            //создание цепей и поиск связи между цепями и элементами
            for (int i = 0; i < cheme.Elements.Count(); i++)
            {
                for (int j = i; j < cheme.Elements.Count(); j++)
                {
                    if (cheme.Contiguity[i][j] == 0)
                        continue;

                    Cheme.CountFactConnetion++;

                    Chain item = new Chain() { CountConnection = cheme.Contiguity[i][j] };

                    item.Elements.Add(Cheme.Elements[i]);
                    Cheme.Elements[i].Chains.Add(item);
                    Cheme.Elements[i].Elements.Add(Cheme.Elements[j]);

                    item.Elements.Add(Cheme.Elements[j]);
                    Cheme.Elements[j].Chains.Add(item);
                    Cheme.Elements[j].Elements.Add(Cheme.Elements[i]);

                    Cheme.Chains.Add(item);
                }
            }

            FindCountConectionAllElements();
            //FindConectionElementsAndNodes(cheme.HubMass);
            //FindAllConnectionNodes();
            //CommonElementsNodes();

            //вычисление максимума фактов связи
            Cheme.MaxFactConnetion = ((Cheme.CountElements * (Cheme.CountElements + 1)) / 2);

            //поиск максимума и минимума количества связей            
            Cheme.MaxCountConnetion = Cheme.Elements.Max(x => x.CountConnection);
            Cheme.MinCountConnetion = Cheme.Chains.Min(x => x.CountConnection);

            //вычисление процентного отношения фактов связи 0 - минимум связей 100 - максимально возможное
            double oneP = (Cheme.MaxFactConnetion - Cheme.CountElements) / 100;
            Cheme.CountFactConnetionPercent = (Cheme.CountFactConnetion - Cheme.CountElements) / oneP;

            //построение гистограмм фактов связи и количества связей относительно элементов
            List<int> paramsFC = new List<int>();
            List<int> paramsCC = new List<int>();
            foreach (var el in Cheme.Elements)
            {
                paramsFC.Add(el.FactConnection);
                paramsCC.Add(el.CountConnection);
            }
            Cheme.FactConnetionStat = new StatisticParam(paramsFC);
            Cheme.CountConnetionStat = new StatisticParam(paramsCC);

            //заполнение гистограммы всех вариантов сочитания фактов связей и количества связей
            for (int i = 0; i < Cheme.FactConnetionStat.Histogram.Count(); i++)
            {
                for (int j = 0; j < Cheme.CountConnetionStat.Histogram.Count(); j++)
                {
                    int countoption = 0;

                    for (int k = 0; k < Cheme.CountElements; k++)
                    {
                        if (Cheme.Elements[k].CountConnection == Cheme.CountConnetionStat.Histogram[j][0] && Cheme.Elements[k].FactConnection == Cheme.FactConnetionStat.Histogram[i][0])
                        {
                            countoption++;
                        }
                    }

                    if (countoption > 0)
                        Cheme.HistogramCountAndFactConnetion.Add(new int[] { Cheme.FactConnetionStat.Histogram[i][0], Cheme.CountConnetionStat.Histogram[j][0], countoption });
                }
            }

            Cheme.CConFC = Cheme.Chains.Sum(x => x.CountConnection);
            //Cheme.CConFC /= Cheme.Chains.Count();



            int bufFC;
            int bufCC;

            bufFC = Cheme.FactConnetionStat.Histogram.Last()[0];
            bufCC = Cheme.CountConnetionStat.Histogram.Last()[0];

            for (int i = Cheme.FactConnetionStat.Histogram.Count -2; i > 1; i--)
            {
                if (Cheme.FactConnetionStat.Histogram[i][1] > 0)
                {
                    Cheme.listOFDeferenceFC.Add(bufFC - Cheme.FactConnetionStat.Histogram[i][0]);
                    bufFC = Cheme.FactConnetionStat.Histogram[i][0];
                }                
            }

            for (int i = Cheme.CountConnetionStat.Histogram.Count - 2; i > 1; i--)
            {
                if (Cheme.CountConnetionStat.Histogram[i][1] > 0)
                {
                    Cheme.listOFDeferenceCC.Add(bufCC - Cheme.CountConnetionStat.Histogram[i][0]);
                    bufCC = Cheme.CountConnetionStat.Histogram[i][0];
                }
            }

            //for (int i = 0; i < Cheme.CountConnetionStat.Histogram.Count - 1; i++)
            //{

            //    Cheme.listOFDeferenceCC.Add(Cheme.CountConnetionStat.Histogram[i + 1][0] - Cheme.CountConnetionStat.Histogram[i][0]);
            //}


            //
            //кореляция двух количественных переменных поможет в описании взаимосвязи фактов связи и количества связей
            //stepic.org/lesson/%D0%9F%D0%BE%D0%BD%D1%8F%D1%82%D0%B8%D0%B5-%D0%BA%D0%BE%D1%80%D1%80%D0%B5%D0%BB%D1%8F%D1%86%D0%B8%D0%B8-8086/step/4?unit=1365
        }

        //поиск связей между узлами и элементами
        public void FindConectionElementsAndNodes(string[][] HubMass)
        {
            for (int i = 0; i < HubMass.Length; i++)
            {
                Node newN = new Node();
                for (int j = 0; j < HubMass[i].Length; j++)
                {
                    Element el = Cheme.Elements.Find(x => x.Name.Contains(HubMass[i][j]));
                    el.Nodes.Add(newN);
                    newN.Elements.Add(el);
                }
                Cheme.Nodes.Add(newN);
            }
        }

        //поиск количества связей у всех элементов
        public void FindCountConectionAllElements()
        {
            foreach (var el in Cheme.Elements)
            {
                el.CalculationCountConnection();
            }
        }

        //поиск всех связей нодов между собой
        public void FindAllConnectionNodes()
        {
            foreach (var node in Cheme.Nodes)
            {
                foreach (var el in node.Elements)
                {
                    foreach (var elNode in el.Nodes)
                    {
                        //если ещё нет связи с искомой нодой
                        if (elNode != node && node.Nodes.IndexOf(elNode) != 1)
                        {
                            node.Nodes.Add(elNode);
                        }
                    }
                }
            }
        }

        //поиск общих элементов между нодами
        public void CommonElementsNodes()
        {

            foreach (var node in Cheme.Nodes)
            {
                node.GenerateBit();
            }

            foreach (var el in Cheme.Elements)
            {
                foreach (var elNodeA in el.Nodes)
                {
                    foreach (var elNodeB in el.Nodes)
                    {
                        if (elNodeB != elNodeA)
                        {
                            elNodeB.CommonElementsNodes[elNodeB.Nodes.IndexOf(elNodeA)].Add(el);
                            //elNodeA.CommonElementsNodes[elNodeA.Nodes.IndexOf(elNodeB)].Add(el);
                        }
                    }
                }

            }
        }
    }

}
