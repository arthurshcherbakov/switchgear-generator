﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testFacotConect.Model
{

    public class SchemeGenerator
    {
        int[][] _cheme;

        public List<Element> Elements;
        public List<Chain> Chains;
        public List<RangeItem> RangeItems;
        //List<int> AddFCDouble;
        public int[][] Cheme
        {
            get { return _cheme; }
        }

        //функция расшифровки
        public void GenerateCheme()
        {
            //bool sdsd = ReferenceEquals(Elements[0], Elements[0].RangeItem.Elements[0]);

            // StepOne();
            //  StepOneNew();

            StepOneNewNEW();

            StepTwoNewNEW();
            StepThreeNEW();

            AddCC();






            //StepThree();

            ////AddFCDouble = new List<int>();

            //////создание массива в котом будем хранить сколько ещё можно добавить в диапозон
            ////for (int i = 0; i < RangeItems.Count; i++)
            ////{
            ////    AddFCDouble.Add(RangeItems[i].GoalFC);
            ////}
            //StepTwo();

            //построение 
            _cheme = new int[Elements.Count][];
            for (int i = 0; i < Elements.Count; i++)
            {
                _cheme[i] = new int[Elements.Count];
                for (int j = 0; j < Elements.Count; j++)
                {
                    _cheme[i][j] = 0;
                }
            }
            //заполнение
            for (int i = 0; i < Chains.Count; i++)
            {
                int line = Elements.IndexOf(Chains[i].Elements[0]);
                int col = Elements.IndexOf(Chains[i].Elements[1]);

                //_cheme[line][col]++;
                //_cheme[col][line]++;

                _cheme[line][col] = Chains[i].CountConnection +1;
                _cheme[col][line]= Chains[i].CountConnection +1;
            }
        }

        //какие данные

        //где хранить сколько фс нужно дабавить в диапозон у айтема или сдесь ?

        //случайно выбрать элемент составить для него список не соединённых элементов
        //из этого списка случайно выбрать элемент 
        //добавить связь*
        //удалить элемент из списка не соединённых
        //проверить второй элемнт на минимум фс
        //продолжать пока не будет достигнут минимум фс для элемента 


        /// <summary>
        /// добовление КС. сейчас оно работает на основании текущего алгортма генерации
        /// , т.е. у последнего диапозоно все элементы связанны между собой
        /// 
        /// </summary>
        private void AddCC()
        {
            Random rnd = new Random();

            List<int> freeRange = new List<int>();

            for (int i = 0; i < RangeItems.Count; i++)
            {
                if (RangeItems[i].AddCC != 0)
                {
                    freeRange.Add(i);
                }
            }

            //цикл по freeRange который исчезает
            while (freeRange.Count != 0)
            {
                //выбрать следующий доступный диапозон
                int correctRange = freeRange[0];

                List<Chain> accessChain = new List<Chain>();
                //заполнение списка доступных чейнов
                foreach (var chain in Chains)
                {
                    
                    //bool AAA = freeRange.IndexOf(RangeItems.IndexOf(chain.Elements[0].RangeItem)) != -1;
                    //bool BBB = freeRange.IndexOf(RangeItems.IndexOf(chain.Elements[1].RangeItem)) != -1;

                    // если два элемента из чейна присутствуют в списке доспунных диапозонов
                    if (freeRange.IndexOf( RangeItems.IndexOf(chain.Elements[0].RangeItem)) != -1
                        && freeRange.IndexOf(RangeItems.IndexOf(chain.Elements[1].RangeItem)) != -1)
                    {
                        accessChain.Add(chain);
                    }
                }

                //затычка на случай если всего один элемент в диапозоне
                if (accessChain.Count == 0)
                {
                    break;
                }
                Chain randChain = accessChain[rnd.Next(accessChain.Count-1)];

                randChain.CountConnection++;

                randChain.Elements[0].RangeItem.AddCC--;
                randChain.Elements[1].RangeItem.AddCC--;

                //проверяем все дипозоны на отсутствие дополнительных КС и удаляем их из доступных
                for (int i = freeRange.Count -1; i >= 0; i--)
                {
                    if (RangeItems[freeRange[i]].AddCC <=0 )
                    {
                        freeRange.RemoveAt(i);
                    }
                }
            }
        }

        private void StepOneNewNEW()
        {
            // List<List<Element>> ElNotHaveMin = new List<List<Element>>();
            //// List<int> freeRangeItems = new List<int>();

            // for (int i = 0; i < RangeItems.Count; i++)
            // {


            //    // freeRangeItems.Add(i);
            //     ElNotHaveMin.Add(new List<Element>());

            //     for (int j = 0; j < RangeItems[i].Elements.Count; j++)
            //     {
            //         ElNotHaveMin[i].Add(RangeItems[i].Elements[j]);
            //     }
            // }

           

            //выбор элемента для которого нужно удовлетворить минимум 
            for (int i = RangeItems.Count - 1; i >= 0; i--)
            {


                for (int j = 0; j < RangeItems[i].Elements.Count; j++)
                {
                    bool ElFullMinChains = false;
                    Element El = RangeItems[i].Elements[j];


                    if (El.Chains.Count == El.RangeItem.LeftRange)
                    {
                        continue;
                    }

                    //сначала связатся со всеми из своего диапозона 
                    //потом левее и левее 
                    for (int k = RangeItems.Count - 1; k >= 0; k--)
                    {
                        for (int n = 0; n < RangeItems[k].Elements.Count; n++)
                        {
                            Element Ell = RangeItems[k].Elements[n];

                            //определить доступные элементы
                            //не достигнут минимум
                            //связи не было
                            if (Ell != El
                                && El.Elements.IndexOf(Ell) == -1
                                && Ell.Chains.Count < Ell.RangeItem.LeftRange)
                            {
                                AddFCTwoElements(El, Ell, false);
                            }

                            if (El.Chains.Count == El.RangeItem.LeftRange)
                            {
                                ElFullMinChains = true;
                                break;
                            }
                        }
                        if (ElFullMinChains)
                        {
                            break;
                        }
                    }
                    //if (ElFullMinChains)
                    //{
                    //    break;
                    //}
                }
            }
        }


        //заполненеи элементов связями до миннимума соответсвующего диапозона
        private void StepOneNew()
        {
            List<Element> ElNotHaveMin = new List<Element>();

            //List<int> freeRangeItems = new List<int>();

            foreach (var item in Elements)
            {
                ElNotHaveMin.Add(item);
            }


            Random rnd = new Random();

            //можно определить изначально сколько останется не занятых (0 либо 1)
            int sumMust = 0;
            for (int i = 0; i < RangeItems.Count; i++)
            {
                sumMust += RangeItems[i].LeftRange * RangeItems[i].Elements.Count;
            }

            int endFlag = sumMust % 2;

            while (ElNotHaveMin.Count != endFlag)
            {
                int ElIndex = rnd.Next(ElNotHaveMin.Count);

                Element El = ElNotHaveMin[ElIndex];

                //список не соединённых
                List<Element> ElForConnect = new List<Element>();
                for (int i = 0; i < ElNotHaveMin.Count; i++)
                {
                    //проверка что нет связи 
                    //проверка что не тот же самый элемент

                    if (ElIndex != i)
                    {
                        if (!El.HaveFC(ElNotHaveMin[i]))
                        {
                            ElForConnect.Add(ElNotHaveMin[i]);
                        }
                    }
                }

                for (int i = 0; i < El.RangeItem.LeftRange; i++)
                {
                    if (ElForConnect.Count == 0)
                    {
                        break;
                    }

                    int EllIndex = rnd.Next(ElForConnect.Count);

                    Element Ell = ElForConnect[EllIndex];



                    AddFCTwoElements(El, Ell, false);

                    //удалить Ell из списка ElForConnect 
                    ElForConnect.Remove(Ell);
                    //и проверить его на заполненность минимума для соотв. диапозона 
                    //и в этом случае удалить из ElNotHaveMin
                    if (Ell.Chains.Count == Ell.RangeItem.LeftRange)
                    {
                        ElNotHaveMin.Remove(Ell);
                    }
                }//for

                ElNotHaveMin.Remove(El);


            }
        }


        void StepTwoNewNEW()
        {

            //поиск оставшихся от перевого шага
            List<Element> notUdov = new List<Element>();

            for (int i = 0; i < Elements.Count; i++)
            {
                if (Elements[i].Chains.Count < Elements[i].RangeItem.LeftRange)
                {
                    notUdov.Add(Elements[i]);
                }
            }

            for (int i = 0; i < notUdov.Count; i++)
            {
                bool ElFullMinChains = false;
                bool CurentRangeHaveMaxChanins = false;
                Element El = notUdov[i];

                if (El.Chains.Count == El.RangeItem.LeftRange || El.RangeItem.AddFCDouble <= 0)
                {
                    continue;
                }

                for (int j = 0; j < RangeItems.Count; j++)
                {
                    for (int n = 0; n < RangeItems[j].Elements.Count; n++)
                    {
                        if (RangeItems[j].AddFCDouble <= 0)
                        {
                            continue;
                        }

                        Element Ell = RangeItems[j].Elements[n];

                        if (Ell != El
                                && El.Elements.IndexOf(Ell) == -1
                                && Ell.Chains.Count < Ell.RangeItem.RightRange)
                        {
                            AddFCTwoElements(El, Ell, true);
                        }

                        if (El.Chains.Count == El.RangeItem.LeftRange)
                        {
                            ElFullMinChains = true;
                            break;
                        }

                        if (El.RangeItem.AddFCDouble <= 0)
                        {
                            CurentRangeHaveMaxChanins = true;
                            break;
                        }
                    }

                    if (CurentRangeHaveMaxChanins || ElFullMinChains)
                    {
                        break;
                    }
                }

            }



        }


        //заполнение оставшехся не заполненных элементов за счёт добавочного ФС
        void StepTwoNew()
        {
            //недоработка не достаточно смотреть на максимум их диаозона 
            //нужно также следить за отсутствием переполнения свободных ФС в диапозоне


            //поиск оставшихся от перевого шага
            List<Element> notUdov = new List<Element>();

            for (int i = 0; i < Elements.Count; i++)
            {
                if (Elements[i].Chains.Count < Elements[i].RangeItem.LeftRange)
                {
                    notUdov.Add(Elements[i]);
                }
            }

            //используется двумерный массив для исключения элементов в случае кода у диапозона закончились свободные ФС
            List<List<Element>> ElNotHaveMin = new List<List<Element>>();
            List<int> freeRangeItems = new List<int>();

            //составления списка элементов с которым можно соединить
            for (int i = 0; i < RangeItems.Count; i++)
            {
                List<Element> Els = new List<Element>();

                //если нет добавочных фс
                if (RangeItems[i].GoalFC <= 0)
                {
                    continue;
                }
                //исключение элементов для которых нужно найти соединение
                for (int j = 0; j < RangeItems[i].Elements.Count; j++)
                {
                    if (notUdov.IndexOf(RangeItems[i].Elements[j]) == -1)
                    {
                        Els.Add(RangeItems[i].Elements[j]);
                    }
                }

                if (Els.Count > 0)
                {
                    freeRangeItems.Add(i);
                    ElNotHaveMin.Add(Els);
                }
            }



            Random rnd = new Random();


            for (int i = 0; i < notUdov.Count; i++)
            {

                //сделать если у текущего диапозонаосталась одна связь то не соединять элементы с этим дапозоном

                List<Element> ElCanConnect = new List<Element>();

                Element El = notUdov[i];

                for (int j = 0; j < ElNotHaveMin.Count; j++)
                {
                    for (int n = 0; n < ElNotHaveMin[j].Count; n++)
                    {
                        //исключить элементы с которыми уже соединён элемент
                        if (notUdov[i].Elements.IndexOf(ElNotHaveMin[j][n]) == -1)
                        {
                            ElCanConnect.Add(ElNotHaveMin[j][n]);
                        }
                    }
                }

                for (int k = 0; k < notUdov[i].RangeItem.LeftRange - notUdov[i].Chains.Count; k++)
                {


                    int EllIndex = rnd.Next(ElCanConnect.Count);
                    Element Ell = ElCanConnect[EllIndex];

                    AddFCTwoElements(Ell, El, true);

                    //удаление элемента из списка доступных и при необходимости из общего списка
                    ElCanConnect.RemoveAt(EllIndex);

                    if (Ell.Chains.Count == Ell.RangeItem.RightRange)
                    {
                        for (int g = 0; g < ElNotHaveMin.Count; g++)
                        {
                            int indexForDelet = ElNotHaveMin[g].IndexOf(Ell);
                            if (indexForDelet != -1)
                            {
                                ElNotHaveMin[g].RemoveAt(indexForDelet);
                                break;
                            }
                        }
                    }

                    //если закончились элементы для текущего элемента
                    if (ElCanConnect.Count == 0)
                    {
                        break;
                    }
                }
            }
        }

        private void StepOne()
        {
            List<List<Element>> ElNotHaveMin = new List<List<Element>>();
            List<int> freeRangeItems = new List<int>();

            for (int i = 0; i < RangeItems.Count; i++)
            {
                freeRangeItems.Add(i);
                ElNotHaveMin.Add(new List<Element>());

                for (int j = 0; j < RangeItems[i].Elements.Count; j++)
                {
                    ElNotHaveMin[i].Add(RangeItems[i].Elements[j]);
                }
            }


            Random rnd = new Random();

            // если остался одина дырка
            bool oneEl = false;

            for (int i = 0; i < RangeItems.Count; i++)
            {

                //случайный выбор диапозона 
                //случайный выбор элемента
                //проверка на отсутсвие совпадения элемента
                //впоследствии сменить на что то более осмысленное

                //отсеивание элементов

                do
                {
                    Element Ell = ElNotHaveMin[i][0];

                    for (int h = 1; h < ElNotHaveMin[i].Count; h++)
                    {
                        if (Ell.Chains.Count > ElNotHaveMin[i][h].Chains.Count)
                        {
                            Ell = ElNotHaveMin[i][h];
                        }
                    }

                    ////заполнение массива элементов в которых отсутсвует связь
                    //List<Element> ElWithoutConnect = new List<Element>();

                    //for (int d = 0; d < ElNotHaveMin.Count; d++)
                    //{
                    //    for (int z = 0; z < ElNotHaveMin[d].Count; z++)
                    //    {
                    //        if (ElNotHaveMin[d][z].Chains.Count > 0)
                    //        {
                    //            if (!Ell.HaveFC(ElNotHaveMin[d][z]))
                    //            {
                    //                ElWithoutConnect.Add(ElNotHaveMin[d][z]);
                    //            }
                    //        }
                    //        else 
                    //        {
                    //            ElWithoutConnect.Add(ElNotHaveMin[d][z]);
                    //        }
                    //    }
                    //}


                    Element El = null;










                    int RI;
                    bool endFindRandomEl = false;
                    do
                    {
                        RI = freeRangeItems[rnd.Next(freeRangeItems.Count)];
                        El = ElNotHaveMin[RI][rnd.Next(ElNotHaveMin[RI].Count)];

                        endFindRandomEl = ReferenceEquals(El, Ell);

                        //проверка, нескем связать элемент
                        if (endFindRandomEl)
                        {
                            if (freeRangeItems.Count == 1 && ElNotHaveMin[freeRangeItems[0]].Count == 1)
                            {
                                oneEl = true;
                                break;
                            }
                        }

                        //если случайно выбранный элемент совподает с ранее выбраным 
                    } while (ReferenceEquals(El, Ell));

                    if (oneEl)
                    {
                        break;
                    }

                    //чеинс должен быть добавлен в главный лист, у каждого(двух) элемента, и в диапозон 

                    Chain CH = new Chain();
                    Chains.Add(CH);
                    RangeItems[i].Chains.Add(CH);
                    if (i != RI)
                    {
                        RangeItems[RI].Chains.Add(CH);
                    }

                    El.Chains.Add(CH);
                    Ell.Chains.Add(CH);

                    CH.Elements.Add(El);
                    CH.Elements.Add(Ell);

                    El.Elements.Add(Ell);
                    Ell.Elements.Add(El);

                    //удаление элемента из списка ElNotHaveMin если он имеет достаточный минимум
                    if (Ell.Chains.Count == RangeItems[i].LeftRange)
                    {
                        ElNotHaveMin[i].Remove(Ell);
                    }

                    if (El.Chains.Count == RangeItems[RI].LeftRange)
                    {
                        ElNotHaveMin[RI].Remove(El);
                    }

                    if (ElNotHaveMin[i].Count == 0)
                    {
                        freeRangeItems.Remove(i);
                    }

                    if (ElNotHaveMin[RI].Count == 0)
                    {
                        freeRangeItems.Remove(RI);
                    }


                } while (ElNotHaveMin[i].Count != 0);
                if (oneEl)
                {
                    break;
                }


            }// for
        }

        //private void StepThreeNEW_two()
        //{
        //    for (int i = RangeItems.Count - 1; i >= 0; i--)
        //    {
        //        for (int j = i; j >= 0; j--)
        //        {

        //        }
        //    }
        //}

        private void StepThreeNEW()
        {
            // последовательынй перебор диапозлонов с конца в начало
            // максмально связать диапозон с всеми до полного истощения ФС у второго

            for (int i = RangeItems.Count - 1; i >= 0; i--)
            {
                //диапозон Х - это диапозон который в данный момент связывается 
                //диапозон У - тот с которым связывается  

                //нужны списки элементов 
                //элементы Х котрорые ещё могут быть связанны с текущим диапозоном У, 
                //перерождается с каждым новом диапозоном У

                //элементы Х котрорые не имеют максимум ФС для своего диапозона
                List<Element> ElNotMaxFC = RangeItems[i].Elements.Where(x => x.Chains.Count < x.RangeItem.RightRange).ToList();

                if (RangeItems[i].AddFCDouble <= 0 || ElNotMaxFC.Count <= 0)
                {
                    continue;
                }


                bool endFCRangeX = false;

                for (int j = i; j >= 0; j--)
                {


                    if (i == j)
                    {
                        foreach (var el in ElNotMaxFC)
                        {
                            if (el.Chains.Count >= el.RangeItem.RightRange)
                            {
                                continue;
                            }

                            foreach (var ell in ElNotMaxFC)
                            {
                                if (ell.Chains.Count >= ell.RangeItem.RightRange)
                                {
                                    continue;
                                }

                                if (el != ell
                                    && el.Elements.IndexOf(ell) == -1)
                                {
                                    //затычка . переделать так чтобы элементы удалялись из списка доступных

                                    if (el.Chains.Count >= el.RangeItem.RightRange || ell.Chains.Count >= ell.RangeItem.RightRange) continue;

                                    AddFCTwoElements(el, ell, true);

                                    //if (el.Chains.Count == 31 || ell.Chains.Count == 31)
                                    //{
                                    //    int ghjk = 2;
                                    //}



                                    //if (el.Chains.Count >= el.RangeItem.RightRange)
                                    //{
                                    //    //скорее всего нельзя удалять элемент в лесте который перебирается 
                                    //    ElNotMaxFC.Remove(el);
                                    //}

                                    //if (ell.Chains.Count >= ell.RangeItem.RightRange)
                                    //{
                                    //    //скорее всего нельзя удалять элемент в лесте который перебирается 
                                    //    ElNotMaxFC.Remove(ell);
                                    //}
                                }

                                //выход в случае если не осталость элементов с не максимальным ФС 
                                if (ElNotMaxFC.Count <= 1)
                                {
                                    endFCRangeX = true;
                                    break;
                                }

                                // у диапозона Х закончились фс
                                if (RangeItems[i].AddFCDouble <= 0)
                                {
                                    endFCRangeX = true;
                                    break;
                                }

                            }// форич перебор с кем соединить элемент
                            if (endFCRangeX)
                            {
                                break;
                            }
                        }//форич перебор элемента который нужно соединить с кем нибудь

                        ElNotMaxFC = RangeItems[i].Elements.Where(x => x.Chains.Count < x.RangeItem.RightRange).ToList();

                        if (endFCRangeX)
                        {
                            break;
                        }


                    } // if X = Y
                    else
                    {
                        List<Element> EllNotMaxFC = RangeItems[j].Elements.Where(x => x.Chains.Count < x.RangeItem.RightRange).ToList();

                        if (RangeItems[j].AddFCDouble <= 0 || EllNotMaxFC.Count <= 0)
                        {
                            continue;
                        }

                        //список не максимальных и на основе него список доступных для соединения с Х


                        //List<Element> EllAllowConect = new List<Element>();

                        bool endFCRangeY = false;

                        foreach (var ell in EllNotMaxFC)
                        {
                            // может ли el соединится с кем нибудь из ElNotMaxFC

                            bool ellMaxFC = false;
                            //foreach (var el in ElNotMaxFC)
                            for (int f = ElNotMaxFC.Count - 1; f >= 0; f--)
                            {
                                Element el = ElNotMaxFC[f];




                                if (ell.Elements.IndexOf(el) == -1)
                                {


                                    AddFCTwoElements(el, ell, true);

                                    //проверки 
                                    // удаление елемента из списка не максимальных диапозона Х
                                    if (el.Chains.Count >= el.RangeItem.RightRange)
                                    {
                                        //скорее всего нельзя удалять элемент в лесте который перебирается 
                                        ElNotMaxFC.Remove(el);
                                    }

                                    //
                                    if (ell.Chains.Count >= ell.RangeItem.RightRange)
                                    {
                                        ellMaxFC = true;
                                        //break;
                                    }

                                    // у первого диапозона закончились фс
                                    if (RangeItems[i].AddFCDouble <= 0)
                                    {
                                        endFCRangeX = true;
                                        //break;
                                    }
                                    // у второго диапозона закончились фс
                                    if (RangeItems[j].AddFCDouble <= 0)
                                    {
                                        endFCRangeY = true;
                                        //break;
                                    }
                                }

                                if (endFCRangeY || endFCRangeX || ellMaxFC)
                                {
                                    break;
                                }


                            }// фор по элементам диапозона Х

                            if (endFCRangeY || endFCRangeX || ellMaxFC)
                            {
                                break;
                            }

                        }// форич по элементам диапозона Y

                    }//else Х!= Y

                }// фор по диапозонам У 

                if (endFCRangeX)
                {
                    continue;
                    //break;
                }
            }// фор по диапозонам X 
        }

        private void StepThree()
        {
            //случайный выбор диапозона 
            //случайный выбор элемента
            //определение для него свободных элементов на основании отсутсвия связи и не заполненности диапозона
            //создание связи

            //предварительный выбор элементов без максимума ФС в соответсвующем диапозоне

            //естли в диапозоне вообще элементы для связывания

            List<List<Element>> ElNotHaveMax = new List<List<Element>>();
            List<int> freeRangeItems = new List<int>();

            for (int i = 0; i < RangeItems.Count; i++)
            {
                List<Element> ElRangeNotMax = new List<Element>();

                //есть ли у диаозона свободные ФС    
                if (RangeItems[i].AddFCDouble > 0)
                {
                    for (int j = 0; j < RangeItems[i].Elements.Count; j++)
                    {
                        if (RangeItems[i].Elements[j].Chains.Count < RangeItems[i].RightRange)
                        {
                            ElRangeNotMax.Add(RangeItems[i].Elements[j]);
                        }
                    }
                }

                if (ElRangeNotMax.Count > 0)
                {
                    freeRangeItems.Add(i);
                    ElNotHaveMax.Add(ElRangeNotMax);
                }
            }

            // усливия выхода 
            //у диапозонов закончились фс
            //закончились элементы с не максимальным ФС для соответсвующего диапозона

            Random rnd = new Random();

            while (freeRangeItems.Count > 0)
            {
                //случайный выбор элемента из последнего диапозона
                int indexLastRange = freeRangeItems.Last();

                int indexEl = rnd.Next(ElNotHaveMax[indexLastRange].Count);

                Element El = ElNotHaveMax[indexLastRange][indexEl];

                //опеределение доступных элементов
                List<Element> accessElements = new List<Element>();

                for (int i = 0; i < freeRangeItems.Count; i++)
                {
                    for (int j = 0; j < ElNotHaveMax[freeRangeItems[i]].Count; j++)
                    {
                        //тотже самый элемент
                        if (indexLastRange == freeRangeItems[i] && j == indexEl)
                        {
                            continue;
                        }

                        Element Ell = ElNotHaveMax[freeRangeItems[i]][j];

                        // проверка наличия связи
                        if (El.Elements.IndexOf(Ell) == -1)
                        {
                            accessElements.Add(Ell);
                        }
                    }
                }

                //выбор случайного элемента из доступных
                Element Elll = accessElements[rnd.Next(accessElements.Count)];

                AddFCTwoElements(El, Elll, true);

                //два раза одно и тоже 
                //проверка что элемент имеет максимум для своего диапозона

                if (Elll.Chains.Count == Elll.RangeItem.RightRange)
                {
                    for (int g = 0; g < ElNotHaveMax.Count; g++)
                    {
                        int indexForDelet = ElNotHaveMax[g].IndexOf(Elll);
                        if (indexForDelet != -1)
                        {
                            ElNotHaveMax[g].RemoveAt(indexForDelet);
                            break;
                        }
                    }
                }

                if (El.Chains.Count == El.RangeItem.RightRange)
                {
                    for (int g = 0; g < ElNotHaveMax.Count; g++)
                    {
                        int indexForDelet = ElNotHaveMax[g].IndexOf(El);
                        if (indexForDelet != -1)
                        {
                            ElNotHaveMax[g].RemoveAt(indexForDelet);
                            break;
                        }
                    }
                }

                //удаление диапозона из доступных на основании что все элементы у диапозона заняты
                //израсходования свободных ФС 

                for (int i = 0; i < ElNotHaveMax.Count; i++)
                {
                    int indexRangeForDel = freeRangeItems.IndexOf(i);

                    if (indexRangeForDel != -1)
                    {
                        if (ElNotHaveMax[i].Count == 0 || RangeItems[indexRangeForDel].AddFCDouble <= 0)
                        {
                            freeRangeItems.RemoveAt(indexRangeForDel);
                        }
                    }
                }

            }
        }

        void StepTwo()
        {
            List<List<Element>> ElNotHaveMin = new List<List<Element>>();
            List<int> freeRangeItems = new List<int>();
            //List<int> AddFCDouble = new List<int>();
            //foreach (var item in RangeItems)
            //{
            //    AddFCDouble.Add(item.GoalFC);
            //}

            for (int i = 0; i < RangeItems.Count; i++)
            {
                freeRangeItems.Add(i);
                ElNotHaveMin.Add(new List<Element>());

                for (int j = 0; j < RangeItems[i].Elements.Count; j++)
                {
                    ElNotHaveMin[i].Add(RangeItems[i].Elements[j]);
                }
            }


            Random rnd = new Random();


            //проблема последнего элемента 
            //проблема последней связи в диапозоне, минус 2 связи когда есть только одна


            while (freeRangeItems.Count != 0)
            {
                //выбрать 2 элемента из двух диапозонов

                int rangeOne = rnd.Next(freeRangeItems.Count);
                int rangeTwo = rnd.Next(freeRangeItems.Count);

                int rangeOneIndex = freeRangeItems[rangeOne];
                int rangeTwoIndex = freeRangeItems[rangeTwo];


                int ElOne = rnd.Next(ElNotHaveMin[rangeOneIndex].Count);
                int ElTwo = rnd.Next(ElNotHaveMin[rangeTwoIndex].Count);

                if (rangeOne == rangeTwo && ElOne == ElTwo)
                {
                    continue;
                }

                Element El = ElNotHaveMin[rangeOneIndex][ElOne];
                Element Ell = ElNotHaveMin[rangeTwoIndex][ElTwo];

                Chain CH = new Chain();

                CH.Elements.Add(El);
                CH.Elements.Add(Ell);

                El.Chains.Add(CH);
                Ell.Chains.Add(CH);

                Chains.Add(CH);

                RangeItems[rangeOneIndex].Chains.Add(CH);
                if (rangeOneIndex != rangeTwoIndex)
                {
                    RangeItems[rangeTwoIndex].Chains.Add(CH);
                }

                //проверки и удаление

                //AddFCDouble[rangeOneIndex]--;
                //AddFCDouble[rangeTwoIndex]--;

                //if (AddFCDouble[rangeOneIndex] <= 0)
                //{
                //    freeRangeItems.RemoveAt(rangeOne);
                //}

                //if (rangeOneIndex != rangeTwoIndex && AddFCDouble[rangeTwoIndex] <= 0)
                //{
                //    freeRangeItems.RemoveAt(rangeTwo);
                //}
            }




        }


        /// <summary>
        /// Добавление ФС между двумя указанными элементами
        /// </summary>
        /// <param name="El"></param>
        /// <param name="Ell"></param>
        /// <param name="flagStep"> true - за счёт ФС диапозона </param>
        void AddFCTwoElements(Element El, Element Ell, bool flagStep)
        {
            Chain CH = new Chain();

            CH.Elements.Add(El);
            CH.Elements.Add(Ell);

            El.Chains.Add(CH);
            Ell.Chains.Add(CH);

            Chains.Add(CH);

            El.Elements.Add(Ell);
            Ell.Elements.Add(El);

            El.RangeItem.Chains.Add(CH);
            if (El.RangeItem != Ell.RangeItem)
            {
                Ell.RangeItem.Chains.Add(CH);
            }

            if (flagStep)
            {
                El.RangeItem.AddFCDouble--;
                Ell.RangeItem.AddFCDouble--;
            }


        }

        public SchemeGenerator()
        {
            RangeItems = new List<RangeItem>();
            Chains = new List<Chain>();
            Elements = new List<Element>();



        }



    }
}
