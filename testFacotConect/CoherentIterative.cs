﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace testFacotConect
{
    public class CoherentIterative
    {
        private int[][] _matrixR; // matrix of location elements on microcircuit
        private int[][] contiguity; // matrix of count connection between elements
        private int _sizeSquareMatrix;
        private int countElements;
        private double _qualityBefore;
        private double _qualityAfter;

        public int SizeSquareMatrix
        {
            get { return this._sizeSquareMatrix; }
        }

        public double QualityBefore
        {
            get { return this._qualityBefore; }
        }

        public double QualityAfter
        {
            get { return this._qualityAfter; }
        }

        public int[][] MatrixR
        {
            get { return this._matrixR; }
        }

        public CoherentIterative(int[][] contiguity)
        {
            this.countElements = contiguity.Length;
            this.contiguity = new int[this.countElements][];


            for (int i = 0; i < this.countElements; i++)
            {
                this.contiguity[i] = new int[this.countElements];

                for (int k = 0; k < this.countElements; k++)
                {
                    this.contiguity[i][k] = contiguity[i][k];
                }
            }
            this._qualityBefore = -1;
            this._qualityAfter = -1;
        }

        public void createMatrixR()
        {
            // first random element             
            int firstElement = new Random().Next(this.countElements);

            //create a matrix with location elements
            _sizeSquareMatrix = Math.Sqrt(this.countElements) == Math.Floor(Math.Sqrt(this.countElements)) ? Convert.ToInt32(Math.Sqrt(this.countElements)) : Convert.ToInt32(Math.Floor(Math.Sqrt(this.countElements))) + 1;

            //создание матрицы R и заполнения её -1 так как идексы елемтов начинаются с 0
            this._matrixR = new int[_sizeSquareMatrix][];
            for (int i = 0; i < _sizeSquareMatrix; i++)
            {
                this._matrixR[i] = new int[_sizeSquareMatrix];
                for (int j = 0; j < _sizeSquareMatrix; j++)
                {
                    this._matrixR[i][j] = -1;
                }
            }
            this._matrixR[0][0] = firstElement;

            int countPutElements = 0;

            for (int i = 0; i < this._sizeSquareMatrix; i++)
            {
                for (int j = 0; j < this._sizeSquareMatrix; j++)
                {
                    if (countPutElements == 0) { countPutElements++; continue; };   // первая ячейка уже занята случайным элементом
                    if (countPutElements == this.countElements) break;
                    countPutElements++;

                    //взять не раставвленный элемент с максимальнм значением дельты
                    int indexNoPutElement = this.getNoPutMaxElement();
                    this._matrixR[i][j] = indexNoPutElement;

                }
            }
            this._qualityBefore = qualityAllLengthEdge();

        } // последовательный этап работы алгоритма

        public void optimizationMatrixR(int countOfSwap) // итерационный этап последовательности алгоритма
        {
            for (int k = 0; k < countOfSwap; k++)
            {
                double[] allMidLengthEdge = new double[this.countElements];
                // получаем средние длины рёбер для всех элентов для текущего состояния матрицы R
                for (int i = 0; i < this.countElements; i++)
                {
                    allMidLengthEdge[i] = getMidLengthEdge(i);
                }
                // нашли все средние рёбра
                int ElementMaxLengthEdge = Array.IndexOf(allMidLengthEdge, allMidLengthEdge.Max()); // элемент с максимальным средним ребром

                // получаем координаты центра тяжести элемента с максимальным значением средней длины ребра
                int[] centreGravity = getCentreGravity(ElementMaxLengthEdge);

                // элементы входящие в пространство центра тяжести 
                int[] ALLcentreGravity = getElementAtCentreGravity(centreGravity);

                //элементы входящие в центра тяжести и смежные с ElementMaxLengthEdge
                IEnumerable<int> elementForChange = ALLcentreGravity.Intersect(this.LocalAdjacency(ElementMaxLengthEdge)[0]);

                // если не удалось отсеять элементы при помощи центра тяжести
                if (elementForChange.Count() == 0)
                    elementForChange = this.LocalAdjacency(ElementMaxLengthEdge)[0];


                // матрицы R создаваемы в результате перестановки элементов 
                int[][][] changeRmatrix = new int[elementForChange.Count()][][];

                //
                double[] deviationOfelementMaxLengthEdge = new double[elementForChange.Count()];
                double[] deviationOfChangeElements = new double[elementForChange.Count()];
                double[] coefficient = new double[elementForChange.Count()];
                //средние длины рёбер для элемнта после перемещения  
                double[] midLengthEdgeElementForChange = new double[elementForChange.Count()];

                for (int i = 0; i < elementForChange.Count(); i++)
                {
                    changeRmatrix[i] = newRmatrixChangeElements(elementForChange.ElementAt(i), ElementMaxLengthEdge);
                    midLengthEdgeElementForChange[i] = getMidLengthEdge(ElementMaxLengthEdge, changeRmatrix[i]);

                    deviationOfelementMaxLengthEdge[i] = allMidLengthEdge.Max() - midLengthEdgeElementForChange[i];
                    deviationOfChangeElements[i] = getMidLengthEdge(elementForChange.ElementAt(i)) - getMidLengthEdge(elementForChange.ElementAt(i)/*ElementMaxLengthEdge*/, changeRmatrix[i]);
                    coefficient[i] = deviationOfelementMaxLengthEdge[i] + deviationOfChangeElements[i];
                }

                int[][] nextRmatrix = changeRmatrix[Array.IndexOf(coefficient, coefficient.Max())];

                //перестановка элементов
                this._matrixR = nextRmatrix;
            }
            this._qualityAfter = qualityAllLengthEdge();
        }

        private int getNoPutMaxElement() //  получить следующий не размещённый элемент удовлетворяющий критерию
        {
            int[] indexesOfPutElements = new int[0];
            int[] indexesOfNonPutElements = new int[0];
            int[] triangle;
            //определить индексы размещённых элементов

            for (int i = 0; i < this._sizeSquareMatrix; i++)
            {
                for (int j = 0; j < this._sizeSquareMatrix; j++)
                {
                    if (this._matrixR[i][j] != -1)
                    {
                        Array.Resize(ref indexesOfPutElements, indexesOfPutElements.Length + 1);
                        indexesOfPutElements[indexesOfPutElements.Length - 1] = this._matrixR[i][j];
                    }

                }
            }

            //indexesOfNonPutElements = new int[this.elements.Length - indexesOfPutElements.Length];
            //получаем массив не размеченных элементов

            for (int i = 0; i < this.countElements; i++)
            {
                bool put = true;

                for (int j = 0; j < indexesOfPutElements.Length; j++)
                {
                    if (i == indexesOfPutElements[j])
                    {
                        put = false;
                    }
                }

                if (put)
                {
                    Array.Resize(ref indexesOfNonPutElements, indexesOfNonPutElements.Length + 1);
                    indexesOfNonPutElements[indexesOfNonPutElements.Length - 1] = i;
                }
            }

            //число ребер, связывающих вершину с ранее размещенными  -   число ребер, инцидентных вер-шине 

            triangle = new int[indexesOfNonPutElements.Length];
            for (int i = 0; i < indexesOfNonPutElements.Length; i++)
            {
                int Aih = 0;
                int Px = 0;

                for (int j = 0; j < indexesOfPutElements.Length; j++)
                {

                    Aih = this.contiguity[indexesOfNonPutElements[i]][indexesOfPutElements[j]] > 0 ?
                        Aih + this.contiguity[indexesOfNonPutElements[i]][indexesOfPutElements[j]] : Aih;
                    //if(this.contiguity[ indexesOfNonPutElements[i], indexesOfPutElements[j] ] > 0)
                    //{
                    //    countRebros += this.contiguity[indexesOfNonPutElements[i], indexesOfPutElements[j]];
                    //}
                }

                for (int j = 0; j < this.countElements; j++)
                {
                    Px += this.contiguity[indexesOfNonPutElements[i]][j];
                }

                triangle[i] = 2 * Aih - Px;
            }

            // в итоге должен получить идекс элемента с максимальным дельтой 
            return indexesOfNonPutElements[Array.IndexOf(triangle, triangle.Max())];
        }
        private int[][] LocalAdjacency(int element) // получить индексы смежных элементов и количество связей с ними
        {
            int[] adjacency = new int[0]; // index of adjacency elements
            int[] CountAdjacency = new int[0]; // count conection between elements 

            for (int i = 0; i < this.countElements; i++)
            {
                if (this.contiguity[element][i] != 0)
                {
                    Array.Resize(ref adjacency, adjacency.Length + 1);
                    adjacency[adjacency.Length - 1] = i;

                    Array.Resize(ref CountAdjacency, CountAdjacency.Length + 1);
                    CountAdjacency[CountAdjacency.Length - 1] = this.contiguity[element][i];
                }
            }
            int[][] result = new int[2][];
            result[0] = adjacency;
            result[1] = CountAdjacency;

            return result;
        }
        public int[] getPositionElementFromRmatrix(int element, int[][] Rmatrix = null) // получить координаты элемента в матрице R 
        {
            if (Rmatrix == null)
            {
                Rmatrix = this._matrixR;
            }

            int[] positionInRmatrix = new int[2];

            for (int i = 0; i < this._sizeSquareMatrix; i++)
            {
                for (int j = 0; j < this._sizeSquareMatrix; j++)
                {
                    if (Rmatrix[i][j] == element)
                    {
                        positionInRmatrix[0] = i;
                        positionInRmatrix[1] = j;

                        return positionInRmatrix;
                    }
                }
            }

            return positionInRmatrix; // затычка
        }

        public int getDuration(int A_element, int B_element, int[][] Rmatrix = null)
        {
            if (Rmatrix == null)
            {
                Rmatrix = this._matrixR;
            }

            int Ax = 0, Ay = 0, Bx = 0, By = 0;

            //получить расстояние 
            for (int i = 0; i < _sizeSquareMatrix; i++)
            {
                if (Array.IndexOf(Rmatrix[i], A_element) != -1)
                {
                    Ay = Array.IndexOf(Rmatrix[i], A_element);
                    Ax = i;
                }
                if (Array.IndexOf(Rmatrix[i], B_element) != -1)
                {
                    By = Array.IndexOf(Rmatrix[i], B_element);
                    Bx = i;
                }
            }
            return Math.Abs(Ax - Bx) + Math.Abs(Ay - By);
        } // получить растояние между элементами в матрице R

        private double qualityAllLengthEdge(int[][] Rmatrix = null)
        {
            if (Rmatrix == null)
            {
                Rmatrix = this._matrixR;
            }

            double result = 0;
            for (int i = 0; i < this.countElements; i++)
            {
                for (int j = i; j < this.countElements; j++)
                {
                    result += this.getDuration(i, j, Rmatrix) * this.contiguity[i][j];
                }
            }
            return result;
        } // оценка качества, средняя длина всех рёбер  

        private double getMidLengthEdge(int element, int[][] Rmatrix = null)
        {
            if (Rmatrix == null)
            {
                Rmatrix = this._matrixR;
            }

            int[][] adjacencyOfElement = LocalAdjacency(element);

            double numerator = 0;
            double denominator = adjacencyOfElement[1].Sum();

            for (int i = 0; i < adjacencyOfElement[0].Length; i++)
            {
                numerator += this.getDuration(element, adjacencyOfElement[0][i], Rmatrix) * adjacencyOfElement[1][i];
            }

            return numerator / denominator;
        } // получить среднию длину ребра

        private int[] getCentreGravity(int element, int[][] Rmatrix = null)
        {
            if (Rmatrix == null)
            {
                Rmatrix = this._matrixR;
            }

            int line = 0, column = 0;

            int[][] adjacency = LocalAdjacency(element);

            for (int i = 0; i < adjacency[0].Length; i++)
            {
                line += getPositionElementFromRmatrix(adjacency[0][i], Rmatrix)[0] * adjacency[1][i];
                column += getPositionElementFromRmatrix(adjacency[0][i], Rmatrix)[1] * adjacency[1][i];
            }

            line /= adjacency[1].Sum();
            column /= adjacency[1].Sum();

            return new int[] { line, column };
        } // получить центр тяжести элемента

        private int[] getElementAtCentreGravity(int[] centreGravity, int[][] Rmatrix = null)
        {
            if (Rmatrix == null)
            {
                Rmatrix = this._matrixR;
            }

            int line = centreGravity[0] - 1;
            int column = centreGravity[1] - 1;

            int[] elementAtCentre = new int[0];

            for (int i = line; i <= centreGravity[0] + 1; i++)
            {
                for (int j = column; j <= centreGravity[1] + 1; j++)
                {
                    if (i >= 0 && j >= 0 && i < this._sizeSquareMatrix && j < this._sizeSquareMatrix && Rmatrix[i][j] != -1)
                    {
                        Array.Resize(ref elementAtCentre, elementAtCentre.Length + 1);
                        elementAtCentre[elementAtCentre.Length - 1] = Rmatrix[i][j];
                    }
                }
            }

            return elementAtCentre;
        }

        private int[][] newRmatrixChangeElements(int A_element, int B_element) // возвращает новую матрицу R в которой 2 элемента меняются местами 
        {
            int[][] newRmatrix = new int[this._sizeSquareMatrix][];

            for (int i = 0; i < this._sizeSquareMatrix; i++)
            {
                newRmatrix[i] = new int[this._sizeSquareMatrix];
                for (int j = 0; j < this._sizeSquareMatrix; j++)
                {
                    newRmatrix[i][j] = this._matrixR[i][j];
                }
            }

            int[] A_coordinates = getPositionElementFromRmatrix(A_element);
            int[] B_coordinates = getPositionElementFromRmatrix(B_element);

            newRmatrix[A_coordinates[0]][A_coordinates[1]] = B_element;
            newRmatrix[B_coordinates[0]][B_coordinates[1]] = A_element;

            return newRmatrix;
        }
    }// class
}// namespase
