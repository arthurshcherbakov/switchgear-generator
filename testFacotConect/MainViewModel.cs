﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using testFacotConect.Controls;

//вопрос какие долны быть диапозоны при авто генерации гистограммы количества связей
//ответ такиеже как и факты связи 

//процесс определения границы черты с которой работает пользователь должно быть опциональным

//процесс определения черты как для ФС так и для КК 
//1 обозначить при помощи слайдера границу с которой работает пользователь 
//2 задать процент который будет содержать в той области с которой работает пользователь




namespace testFacotConect
{
    class MainViewModel : BindableBase
    {
        ObservableCollection<ItemHistogram> _itemsFactOfConnection;
        ObservableCollection<ItemHistogram> _itemsCountConnection;
        bool _enableParam;
        bool _getSideHistogram;
        bool _arightSumCountElement;
        bool _arightBalanceFC;
        int _count;
        int _countElement;
        int _countFactConnection;
        int _maxCountFactConnection;
        int _chemeMaxFC;
        int _rightBorderFC;
        int _freeFCAfterCheckBalance;
        public Model.SchemeGenerator Generator;

        public int[][] mas;

        ItemHistogram _selectedItem;

        public RelayCommand RefreshBottomPanelCommand { get; set; }
        public RelayCommand CheckArightSumCountElement { get; set; }
        public RelayCommand CheckBalanceFC { get; set; }

        public RelayCommand<ItemHistogram> AddItemHistogramCommand { get; set; }
        public RelayCommand<ItemHistogram> RemoveItemHistogramCommand { get; set; }
        public int CountElement
        {
            get { return _countElement; }
            set
            {
                SetProperty(ref _countElement, value);
            }
        }

        //коиличество свободных фс после проверки на баланс, хранится в умноженном на 2 представлении 
        public int FreeFCAfterCheckBalance
        {
            get { return _freeFCAfterCheckBalance; }
            set
            {
                SetProperty(ref _freeFCAfterCheckBalance, value);
            }
        }
        //количество фактов связи
        public int FC
        {
            get { return _countFactConnection; }
            set
            {
                SetProperty(ref _countFactConnection, value);
            }
        }

        //максимум фактов связи
        public int MaxFC
        {
            get { return _maxCountFactConnection; }
            set
            {
                SetProperty(ref _maxCountFactConnection, value);
            }
        }

        // максимум ФС для схемы
        public int MaxChemeFC
        {
            get { return _chemeMaxFC; }
            set
            {
                SetProperty(ref _chemeMaxFC, value);
            }
        }

        //правая граница последнего диапозона
        public int RightBorderFC
        {
            get { return _rightBorderFC; }
            set
            {
                SetProperty(ref _rightBorderFC, value);
            }
        }

        public ItemHistogram SelectedItemHistogram
        {
            get { return _selectedItem; }
            set
            {
                SetProperty(ref _selectedItem, value);
            }
        }
        //свойство получения стороны(право, лево)
        public bool GetSideHistogram
        {
            get
            {
                return _getSideHistogram;
            }
            set
            {
                SetProperty(ref _getSideHistogram, value);
            }
        }

        public bool ArightBalanceFC
        {
            get
            {
                return _arightBalanceFC;
            }
            set
            {
                SetProperty(ref _arightBalanceFC, value);
            }
        }
        public bool ArightSumCountElement
        {
            get
            {
                return _arightSumCountElement;
            }
            set
            {
                SetProperty(ref _arightSumCountElement, value);
            }
        }
        public ObservableCollection<ItemHistogram> ItemsFactOfConnection
        {
            get { return _itemsFactOfConnection; }
            set
            {
                SetProperty(ref _itemsFactOfConnection, value);
            }
        }
        public ObservableCollection<ItemHistogram> ItemsCountConnection
        {
            get { return _itemsCountConnection; }
            set { SetProperty(ref _itemsCountConnection, value); }
        }
        public bool EnableParam
        {
            get { return _enableParam; }
            set { SetProperty(ref _enableParam, value); }
        }


        public int ValueForSeparation
        {
            get { return _count; }
            set
            {
                if (_count != value)
                {
                    SetProperty(ref _count, value);
                }
            }
        }
        public delegate void DoingRefreshVisual();
        public DoingRefreshVisual operation { get; set; }



        public MainViewModel()
        {


            ItemsFactOfConnection = new ObservableCollection<ItemHistogram>();
            ItemsCountConnection = new ObservableCollection<ItemHistogram>();


            RefreshBottomPanelCommand = new RelayCommand(RefreshFunc);
            CheckArightSumCountElement = new RelayCommand(CheckArightSumCountElementFunc);
            CheckBalanceFC = new RelayCommand(CheckBalanceFCFunc);

            ////зададим начальные хуйни 
            //CountElement = 212;
            //FC = 695;
            //RightBorderFC = 56; 

            //ItemsFactOfConnection.Add(new ItemHistogram() 
            //{   
            //    СountElement = CountElement,
            //    HeightColumn = 200, 
            //    Block = false, Start = 3,
            //    End = RightBorderFC, 
            //    WidthProportional = 2.0,
            //    PercentPart = 100
            //});




            //ItemsCountConnection.Add(new ItemHistogram() 
            //{ 
            //    InsideList = true, 
            //    HeightColumn = 101,
            //    Block = false, 
            //    Start = 50, 
            //    End = 100,
            //    WidthProportional = 2.0,
            //    PercentPart = 100 
            //});

            //Функция создающая схему 
            // createCheme();






            SelectedItemHistogram = null;
            AddItemHistogramCommand = new RelayCommand<ItemHistogram>(AddItemHistogram);
            RemoveItemHistogramCommand = new RelayCommand<ItemHistogram>(RemoveItemHistogram);
            EnableParam = true;
            GetSideHistogram = true;

            ItemsFactOfConnection.CollectionChanged += CollectionChangedMethod;
        }

        //функция удлетворияния мимнимума

        //all - (internal + те связи которые удалось соединить)


        void CheckBalanceFCFunc()
        {
            double sumInternal = 0;
            double sumExternal = 0;
            double sumAll = 0;
            foreach (var item in ItemsFactOfConnection)
            {
                double[] FCItem = getFC(item.Start, item.CountElement);
                item.AllFCForItem = Convert.ToInt32(FCItem[0]);
                sumInternal += FCItem[2];
                sumExternal += FCItem[1];
                sumAll += FCItem[0];

            }
            //_freeFCAfterCheckBalance = Convert.ToInt32(((FC - sumInternal) - (sumExternal * 2)) / 2);

            _freeFCAfterCheckBalance = Convert.ToInt32(FC - sumAll + sumExternal / 2);

            ArightBalanceFC = _freeFCAfterCheckBalance >= 0 ? true : false;
        }
        void CheckArightSumCountElementFunc()
        {
            //CountElement 
            int shetch = 0;
            foreach (ItemHistogram item in ItemsFactOfConnection)
            {
                shetch += item.CountElement;
            }

            ArightSumCountElement = shetch == CountElement ? true : false;

        }
        void RefreshFunc()
        {
            RefreshPercentPart(ItemsFactOfConnection[0].InsideList);
            operation();

        }

        double[] getFC(double FC, double CountElement)
        {
            double all = 0;
            double inter = 0;
            double exter = 0;

            if (FC <= CountElement + 1)
            {   //если количество элементов меньше либо равно 
                //чем нужно для полного связывания их между собой
                //all = getMaxFCForelementWihtFC(FC);
                if (FC % 2 == 0)
                {
                    //если количество элементов чётно
                    all += CountElement * FC / 2;
                    inter = all;
                }
                else
                {
                    all += Convert.ToInt32(Math.Ceiling(CountElement * FC / 2));
                    inter = all - 1;
                    exter = 1;
                }
            }
            else
            {
                //фс больше чем элементов
                inter = getMaxFCForelementWihtFC(CountElement);

                //for (int i = 0, j = (int)FC; i < CountElement; i++, j--)
                //{
                //    all += j;
                //}

                //for (int i = (int)FC; i > (int)FC - CountElement; i--)
                //{
                //    all += i;
                //}

                for (int i = (int)FC - (int)CountElement; i < (int)FC; i++)
                {
                    all += i;
                }
                exter = all - inter;

            }

            double[] result = new double[3] { all, exter, inter };

            return result;


        }

        //формула из 3.23
        double getMaxFCForelementWihtFC(double FC)
        {
            return (FC + 1) * FC / 2;
        }


        public void createCheme()
        {
            Generator = new Model.SchemeGenerator();

            mas = new int[CountElement][];
            for (int i = 0; i < CountElement; i++)
            {
                mas[i] = new int[CountElement];
            }

            double sumFC = 0;
            int sumFCLower = 0;

            int RemainderFreeFCAfterCheckBalance = FreeFCAfterCheckBalance;
            //предварительно распределим фс для диапозон 
            //у которых доступный максимум больше чем назначенный процент


            //список элементов для отслеживания свободных диапозонов
            List<int> freeRange = new List<int>();
            //список процентов добавочных ФС, изменяется в процессе выяснения максимумма ФС диапозонов
            List<double> freeRangePercentPart = new List<double>();

            //переделать 
            //на каждой итерации ищаца диапозоны 
            //для которых процент больше чем максимальный ФС диапозона 
            //им ставится максимум и одному диапозону ФС в процентах

            //сначала бесконечный цикл цель которого отсеять диапозоны у которых максимальный дополнительный ФС 
            //больше чем назначенный 

            //а потом присвоение дополниьельных ФС на основании процентов


            for (int i = 0; i < ItemsFactOfConnection.Count; i++)
            {
                //
                freeRange.Add(i);
                //предворительное создание пустого листа для присваивания диапозоно НЕ ПО ПОРЯДКУ
                Generator.RangeItems.Add(null);
                freeRangePercentPart.Add(ItemsFactOfConnection[i].PercentPart);
            }

            while (true)
            {
                //список для удаления
                List<int> RangeForSetMaxFC = new List<int>();
                List<int> floorFC = new List<int>();

                //поиск диапозонов с у которых максимальный дополнительный ФС 
                //больше чем назначенный 
                //отсутствие обозначает выход из цикла 
                for (int i = 0; i < freeRange.Count; i++)
                {
                    int currentRange = freeRange[i];

                    //вычисление добавочного ФС на осовании процентов
                    //double additionalFC = (Convert.ToDouble(FreeFCAfterCheckBalance * 2) / 100)
                    //* ItemsFactOfConnection[currentRange].PercentPart;
                    double additionalFC = (Convert.ToDouble(RemainderFreeFCAfterCheckBalance * 2) / 100)
                        * freeRangePercentPart[currentRange];

                    //вычисление максимума добавочных ФС для диапозона
                    double deferenceEndStart = ItemsFactOfConnection[currentRange].CountElement * ItemsFactOfConnection[currentRange].End
                    - ItemsFactOfConnection[currentRange].CountElement * ItemsFactOfConnection[currentRange].Start;
                    deferenceEndStart *= 2;

                    if (additionalFC > Math.Floor(deferenceEndStart))
                    {
                        //RangeForSetMaxFC.Add(currentRange);
                        RangeForSetMaxFC.Add(i);

                        int flooradditionalFC = Convert.ToInt32(Math.Floor(deferenceEndStart));
                        floorFC.Add(flooradditionalFC);
                    }
                }

                //отсутствие максимальных диапозонов
                if (RangeForSetMaxFC.Count == 0)
                {
                    break;
                }

                //заполнение найденных диапозонов и удаление их из обработки
                for (int i = 0; i < RangeForSetMaxFC.Count; i++)
                {

                    // int currentRange = RangeForSetMaxFC[i];
                    int currentRange = freeRange[RangeForSetMaxFC[i]];

                    RemainderFreeFCAfterCheckBalance -= floorFC[i];

                    //добавление в обьект генератора
                    Model.RangeItem modelRangeItem = new Model.RangeItem();
                                        
                    modelRangeItem.GoalFC = floorFC[i];
                    modelRangeItem.LeftRange = ItemsFactOfConnection[currentRange].Start;
                    modelRangeItem.RightRange = ItemsFactOfConnection[currentRange].End;
                    modelRangeItem.GoalCC = ItemsFactOfConnection[currentRange].СountConnection;


                    for (int j = 0; j < ItemsFactOfConnection[currentRange].CountElement; j++)
                    {
                        Model.Element modelElement = new Model.Element();
                        modelElement.RangeItem = modelRangeItem;
                        modelElement.code = "i:" + i + " j:" + j + " c:" + Generator.Elements.Count;

                        modelRangeItem.Elements.Add(modelElement);
                        Generator.Elements.Add(modelElement);
                    }
                    Generator.RangeItems[currentRange] = modelRangeItem;

                    //удаление диапозонов с максимальных ФС
                    freeRange.RemoveAt(RangeForSetMaxFC[i]);
                }


                //перерасчёт соотношение добавочных фс
                double totalHeight = 0;
                for (int i = 0; i < freeRange.Count; i++)
                {
                    totalHeight += ItemsFactOfConnection[freeRange[i]].HeightColumn;
                }

                double onePercent = totalHeight / 100;

                //for (int i = 0; i < freeRangePercentPart.Count; i++)
                for (int i = 0; i < freeRange.Count; i++)
                {
                    freeRangePercentPart[freeRange[i]] = ItemsFactOfConnection[freeRange[i]].HeightColumn / onePercent;
                }

                //double PercentPart = ItemsFactOfConnection[freeRange[i]].HeightColumn / onePercent;

            }//while поиска максимальных диапозонов 

            //фор который распределяет добавчные фс 
            for (int i = 0; i < freeRange.Count; i++)
            {
                int currentRange = freeRange[i];

                double additionalFC = (Convert.ToDouble(RemainderFreeFCAfterCheckBalance * 2) / 100)
                    * freeRangePercentPart[currentRange];

                //добавление диапозона в генератор
                int flooradditionalFC = Convert.ToInt32(Math.Floor(additionalFC));
                Model.RangeItem modelRangeItem = new Model.RangeItem();
                modelRangeItem.GoalFC = flooradditionalFC;
                modelRangeItem.LeftRange = ItemsFactOfConnection[currentRange].Start;
                modelRangeItem.RightRange = ItemsFactOfConnection[currentRange].End;
                modelRangeItem.GoalCC = ItemsFactOfConnection[currentRange].СountConnection;


                for (int j = 0; j < ItemsFactOfConnection[currentRange].CountElement; j++)
                {
                    Model.Element modelElement = new Model.Element();
                    modelElement.RangeItem = modelRangeItem;
                    modelElement.code = "i:" + i + " j:" + j + " c:" + Generator.Elements.Count;

                    modelRangeItem.Elements.Add(modelElement);
                    Generator.Elements.Add(modelElement);
                }
                Generator.RangeItems[currentRange] = modelRangeItem;

            }


            ////предворительное создание пустого листа для присваивания диапозоно НЕ ПО ПОРЯДКУ
            //for (int i = 0; i < ItemsFactOfConnection.Count; i++)
            //{
            //    Generator.RangeItems.Add(null);
            //}

            //for (int i = ItemsFactOfConnection.Count - 1; i >= 0; i--)
            //{
            //    double additionalFC = (Convert.ToDouble(FreeFCAfterCheckBalance * 2) / 100)
            //        * ItemsFactOfConnection[i].PercentPart;



            //    double deferenceEndStart = ItemsFactOfConnection[i].CountElement * ItemsFactOfConnection[i].End
            //        - ItemsFactOfConnection[i].CountElement * ItemsFactOfConnection[i].Start;

            //    deferenceEndStart *= 2;

            //    //double deferenceEndStart = ItemsFactOfConnection[CurentRange].CountElement * ItemsFactOfConnection[CurentRange].End
            //    //    - ItemsFactOfConnection[CurentRange].CountElement * ItemsFactOfConnection[CurentRange].Start;
            //    //additionalFC = additionalFC > deferenceEndStart ? deferenceEndStart : additionalFC;  
            //    if (additionalFC > Math.Floor(deferenceEndStart))
            //    {
            //        int flooradditionalFC = Convert.ToInt32(Math.Floor(deferenceEndStart));

            //        RemainderFreeFCAfterCheckBalance -= flooradditionalFC;

            //        //добавление в обьект генератора
            //        Model.RangeItem modelRangeItem = new Model.RangeItem();

            //        sumFCLower += flooradditionalFC;

            //        // modelRangeItem.GoalFC = flooradditionalFC;
            //        modelRangeItem.GoalFC = flooradditionalFC;
            //        modelRangeItem.LeftRange = ItemsFactOfConnection[i].Start;
            //        modelRangeItem.RightRange = ItemsFactOfConnection[i].End;
            //        modelRangeItem.GoalCC = ItemsFactOfConnection[i].СountConnection;


            //        for (int j = 0; j < ItemsFactOfConnection[i].CountElement; j++)
            //        {
            //            Model.Element modelElement = new Model.Element();
            //            modelElement.RangeItem = modelRangeItem;
            //            modelElement.code = "i:" + i + " j:" + j + " c:" + Generator.Elements.Count;

            //            modelRangeItem.Elements.Add(modelElement);
            //            Generator.Elements.Add(modelElement);
            //        }

            //        Generator.RangeItems[i] = modelRangeItem;
            //    }
            //    else
            //    {
            //        freeRange.Add(i);
            //    }
            //}


            //for (int i = freeRange.Count - 1; i >= 0; i--)
            //{
            //    double totalHeight = 0;

            //    for (int j = 0; j <= i; j++)
            //    {
            //        totalHeight += ItemsFactOfConnection[freeRange[j]].HeightColumn;
            //    }

            //    double onePercent = totalHeight / 100;

            //    double PercentPart = ItemsFactOfConnection[freeRange[i]].HeightColumn / onePercent;

            //    double additionalFC = (Convert.ToDouble(RemainderFreeFCAfterCheckBalance * 2) / 100) * PercentPart;

            //    double deferenceEndStart = ItemsFactOfConnection[freeRange[i]].CountElement * ItemsFactOfConnection[freeRange[i]].End
            //        - ItemsFactOfConnection[freeRange[i]].CountElement * ItemsFactOfConnection[freeRange[i]].Start;

            //    deferenceEndStart *= 2;


            //    //if (additionalFC > deferenceEndStart)
            //    //{
            //    //    int fghjkuygf = 2;
            //    //}

            //    //добавление диапозона в генератор
            //    int flooradditionalFC = Convert.ToInt32(Math.Floor(additionalFC));
            //    Model.RangeItem modelRangeItem = new Model.RangeItem();

            //    sumFCLower += flooradditionalFC;
            //    modelRangeItem.GoalFC = flooradditionalFC;
            //    modelRangeItem.LeftRange = ItemsFactOfConnection[freeRange[i]].Start;
            //    modelRangeItem.RightRange = ItemsFactOfConnection[freeRange[i]].End;
            //    modelRangeItem.GoalCC = ItemsFactOfConnection[freeRange[i]].СountConnection;


            //    for (int j = 0; j < ItemsFactOfConnection[freeRange[i]].CountElement; j++)
            //    {
            //        Model.Element modelElement = new Model.Element();
            //        modelElement.RangeItem = modelRangeItem;
            //        modelElement.code = "i:" + i + " j:" + j + " c:" + Generator.Elements.Count;

            //        modelRangeItem.Elements.Add(modelElement);
            //        Generator.Elements.Add(modelElement);
            //    }

            //    //Generator.RangeItems.Add(modelRangeItem);
            //    Generator.RangeItems[freeRange[i]] = modelRangeItem;

            //    freeRange.RemoveAt(i);
            //}



            //switchgear generator
            Generator.GenerateCheme();

            using (System.IO.StreamWriter file =
            new System.IO.StreamWriter(@"CHEME_GENERATED.txt"))
            {
                for (int i = 0; i < Generator.Cheme.Length; i++)
                {
                    for (int j = 0; j < Generator.Cheme.Length; j++)
                    {
                        if (i == j)
                        {
                            file.Write("$ ");

                        }
                        else
                            file.Write(Generator.Cheme[i][j] + " ");
                    }
                    file.WriteLine(" ");
                }
            }

        }

        //функция создания матрицы R
        //void createCheme() 
        //{

        //    mas = new int[CountElement][];
        //    for (int i = 0; i < CountElement; i++)
        //    {
        //        mas[i] = new int[CountElement];                
        //    }


        //    //количество элементов заданной ФС [ФС, количество]
        //    List<int[]> HistogramFactOfConnection ;

        //    //обход по элементам c целью определить минимум
        //    foreach (var item in ItemsFactOfConnection)
        //    {
        //        item.AllFCForItem = Convert.ToInt32(getFC(item.Start, item.СountElement));
        //    }

        //    ///////////////////////



        //    //тестовая хуета 

        //    //ItemsFactOfConnection[0].AllFCForItem = FC;
        //    ItemsFactOfConnection[0].InternalFC = FC;

        //    int additionalFC = FC - ItemsFactOfConnection[0].AllFCForItem;

        //    HistogramFactOfConnection = normalMas(
        //        ItemsFactOfConnection[0].Start, 
        //        ItemsFactOfConnection[0].End, 
        //        ItemsFactOfConnection[0].СountElement,
        //        additionalFC);

        //    int konec = 0;

        //    for (int i = 0; i < HistogramFactOfConnection.Count; i++)
        //    {
        //        if (HistogramFactOfConnection[i][1] == 0)
        //        {
        //            konec = i;
        //            break;                    
        //        }
        //    }

        //    //этот лист должен говорить какие элементы есть в распоряжении 
        //    List<int[]> fullMas = new List<int[]>(); //[max FC, curent FC, index] index - из матрицы R

        //    int index = 0;
        //    for (int i = 0; i < konec; i++)
        //    {
        //        for (int j = 0; j < HistogramFactOfConnection[i][1]; j++)
        //        {
        //            fullMas.Add(new int[] { HistogramFactOfConnection[i][0], 0, index });
        //            index++;
        //        }
        //    }

        //    /////


        //    Random rnd = new Random();

        //    for (int i = CountElement - 1; i > 1; i--)
        //    {
        //        List<int> ListOfIndexFreeElements = new List<int>();
        //        for (int t = 0; t < i - 1; t++)
        //        {
        //            if (fullMas[t][0] > fullMas[t][1])
        //            {
        //                ListOfIndexFreeElements.Add(t);
        //            }                    
        //        }

        //        while(fullMas[i][0] > fullMas[i][1])
        //        {
        //            if (0 >= ListOfIndexFreeElements.Count)
        //            {

        //            }

        //            int j = rnd.Next(0, ListOfIndexFreeElements.Count);

        //            int k = ListOfIndexFreeElements[j];

        //            //if (mas[i][k] == 1)
        //            //{

        //            //}
        //            mas[i][k] += 1;
        //            mas[k][i] += 1;


        //            //mas[ fullMas[i][2] ][ fullMas[k][2] ] += 1;

        //            ListOfIndexFreeElements.RemoveAt(j);

        //            fullMas[i][1]++;
        //            fullMas[k][1]++;
        //        }
        //        //ListOfIndexFreeElements.RemoveAt(ListOfIndexFreeElements.Count -1);

        //    }

        //    ////отобразим треугольник 
        //    //for (int i = 0; i < CountElement; i++)
        //    //{

        //    //}


        //    //    //обход по элементам гистограммы c конца с целью 
        //    //for (int i = ItemsFactOfConnection.Count; i > 0; i--)
        //    //{

        //    //}

        //}
        private void CollectionChangedMethod(object sender, NotifyCollectionChangedEventArgs e)
        {
            RefreshPercentPart(((ObservableCollection<ItemHistogram>)sender)[0].InsideList);
        }

        async void RefreshPercentPart(bool input)
        {
            await RefreshPercentPartFunc(input);
        }

        Task<int> RefreshPercentPartFunc(bool input)
        {
            var tast = new Task<int>(() =>
            {
                //сделать блокировку асинхронной функции при расчёте 

                ObservableCollection<ItemHistogram> ListColomns =
                    input == true ? ItemsCountConnection : ItemsFactOfConnection;

                double totalHeight = 0;

                foreach (var item in ListColomns)
                {
                    totalHeight += item.HeightColumn;
                }

                double onePercent = totalHeight / 100;

                foreach (var item in ListColomns)
                {

                    item.PercentPart = item.HeightColumn / onePercent;
                }
                return 0;
            });
            tast.Start();
            return tast;
        }


        void RemoveItemHistogram(ItemHistogram input)
        {
            ObservableCollection<ItemHistogram> AddedList;
            ItemHistogram DelItem = input;
            //определеяю в какой список добовлять 

            //!!!!!!!!!!! переделать логику определения листа либо стандартное значение
            if (!input.InsideList)
            {
                AddedList = ItemsFactOfConnection;
            }
            else
            {
                AddedList = ItemsCountConnection;
            }

            if (AddedList.Count <= 1)
            {
                return;
            }

            //определить естли диапозоны с право и лево
            int index = AddedList.IndexOf(input);

            //есть ли диапозоны слева
            bool haveLeft = AddedList.ToList().FindAll(x => AddedList.IndexOf(x) < index).Count > 0;

            //есть ли диапозоны справа
            bool haveRight = AddedList.ToList().FindAll(x => AddedList.IndexOf(x) > index).Count > 0;

            //true - добавить диапозон в лево, false - добавить диапозон в право
            bool SideDelete = false;

            if (haveLeft || haveRight)
            {
                SideDelete = haveLeft; 
            }
            else 
            {
                //удалить и отдать диапозон в лево 
                if (GetSideHistogram)
                {
                    SideDelete = true;
                }
                ////удалить и отдать диапозон в право
                //else
                //{

                //}
            }

            if (SideDelete)
            {
                AddedList[index - 1].End = input.End;
            }
            else 
            {
                AddedList[index + 1].Start = input.Start;
            }

            ItemsFactOfConnection.RemoveAt(index);


        }

        void AddItemHistogram(ItemHistogram input)
        {
            ObservableCollection<ItemHistogram> AddedList;
            int startRange;
            int endRange;
            //определеяю в какой список добовлять 

            //!!!!!!!!!!! переделать логику определения листа либо стандартное значение
            if (!input.InsideList)
            {
                AddedList = ItemsFactOfConnection;
            }
            else
            {
                AddedList = ItemsCountConnection;
            }

            //на основании выбранной стороны добовляем новый столбец 
            //и присваемаем ему ту сторону часть области которую выбрал пользователь 
            //и изменение значение текущего столбца
            //создать слева
            if (GetSideHistogram)
            {
                startRange = input.Start;
                endRange = ValueForSeparation;

                input.Start = ValueForSeparation + 1;
            }
            //создать справа
            else
            {
                startRange = ValueForSeparation;
                endRange = input.End;

                input.End = ValueForSeparation - 1;
            }

            ItemHistogram addedItem = new ItemHistogram()
            {
                HeightColumn = 101,
                Start = startRange,
                End = endRange,
                Block = false,
                WidthProportional = 2.0,
                InsideList = input.InsideList
            };

            int Index = AddedList.IndexOf(input);
            Index = GetSideHistogram ? Index : Index + 1;
            AddedList.Insert(Index, addedItem);



        }


    }
}
