﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testFacotConect
{
    //это должен быть виртуальный класс в конструктор которого должен передоваться путь к файлу 
    //этот класс должен решать проблему с определением формата файла и выбором класса который преобразует его доступный вид
    //
    //каждый из классов работающий с определённым форматом должен предостовлять двумерный массив 
    //отображающий сколько пинов элемента входит в узел
    public class FileReader
    {
        private SingleFormat _cheme;

        private List<string> _elements; // all elements  
        private string[][] _hubMass; // unique content each hub 
        private int[][] _contiguity; // matrix of count connection between elements
        private int[][] _hubsElementCount; // matrix of count elements in hubs

        public List<string> Elements
        {
            set { _elements = value; }
            get { return this._elements; }
        }
        public string[][] HubMass
        {
            get { return this._hubMass; }
        }
        public int[][] Contiguity
        {
            set { _contiguity = value; }
            get { return this._contiguity; }
        }

        public int[][] HubsElementCount
        {
            get { return this._hubsElementCount; }
        }

        public FileReader(int[][] Contiguity, List<string> listname)
        {
            this.Contiguity = Contiguity;
            this.Elements = listname;
        }

        public FileReader(string path)
        {

            string fileText = System.IO.File.ReadAllText(path);

            //свич кейс определение формата файла
            string primeta = fileText.Substring(0, fileText.IndexOf('\n')).Replace("\r", "");

            switch (primeta)
            {
                case "*OrCAD":
                    _cheme = new OrCADFormat(fileText);
                    break;
                case "$PACKAGES":
                    _cheme = new AllegroFormat(fileText);
                    break;
                default:
                    _cheme = new CL_90Format(fileText);
                    break;
            }

            _elements = new List<string>();
            _hubMass = new string[_cheme.HubsWithAllElement.Length][];

            //заполнение свойств            
            //заполнение _hubMass и _elements
            for (int i = 0; i < _cheme.HubsWithAllElement.Length; i++)
            {
                _hubMass[i] = _cheme.HubsWithAllElement[i].Distinct().ToArray();

                for (int j = 0; j < _hubMass[i].Length; j++)
                {
                    if (!_elements.Contains(_hubMass[i][j]))
                    {
                        _elements.Add(_hubMass[i][j]);
                    }
                }
            }

            //заполнение _contiguity
            _contiguity = new int[_elements.Count()][];
            for (int j = 0; j < this._elements.Count(); j++)
            {
                this._contiguity[j] = new int[_elements.Count()];
            }

            for (int k = 0; k < _hubMass.Length; k++)
            {
                for (int i = 0; i < _hubMass[k].Length; i++)
                {
                    int line = _elements.IndexOf(_hubMass[k][i]);
                    for (int j = 0; j < _hubMass[k].Length; j++)
                    {
                        if (i != j)
                        {
                            int column = _elements.IndexOf(_hubMass[k][j]);
                            _contiguity[line][column]++;
                        }
                    }
                }
            }

            //заполнение _hubsElementCount
            _hubsElementCount = new int[_hubMass.Length][];
            for (int i = 0; i < _hubMass.Length; i++)
            {
                _hubsElementCount[i] = new int[_elements.Count()];

                for (int j = 0; j < _elements.Count(); j++)
                {
                    _hubsElementCount[i][j] = Array.FindAll(_cheme.HubsWithAllElement[i], x => x.Contains(_elements.ElementAt(j))).Length;
                }
            }

        }

    }

    /// <summary>
    /// Данный класс вприводит к общему описанию схемы. Предостовляет двумерный стринговый массив.  
    /// </summary>       
    abstract class SingleFormat
    {
        protected string[][] _hubsWithAllElement;
        public string[][] HubsWithAllElement
        {
            get { return _hubsWithAllElement; }
        }
        protected void RemoveSpaces(ref string txt)
        {
            txt = txt.Replace("  ", " ");

            if (txt.Contains("  "))
                this.RemoveSpaces(ref txt);
        }
    }

    class AllegroFormat : SingleFormat
    {
        public AllegroFormat(string fileText)
        {
            int perv = fileText.IndexOf("$NETS");
            string clearText = fileText.Substring(perv + 7, fileText.Length - (perv + 13)).Replace('\r', ' ').Replace('\n', ' ').Replace(',', ' ');
            RemoveSpaces(ref clearText);
            string[] splitMass = clearText.Split(';');

            _hubsWithAllElement = new string[splitMass.Length - 1][];

            for (int i = 1; i < splitMass.Length; i++)
            {
                int durationOfLine;
                string text;

                durationOfLine = i != splitMass.Length - 1 ? splitMass[i].LastIndexOf(' ') : splitMass[i].Length - 1;

                text = splitMass[i].Substring(1, durationOfLine - 1);
                string[] hub = text.Split(' ');

                _hubsWithAllElement[i - 1] = new string[hub.Length];

                for (int j = 0; j < hub.Length; j++)
                {
                    _hubsWithAllElement[i - 1][j] = hub[j] = hub[j].Split('.')[0];
                }
            }
        }
    }

    class OrCADFormat : SingleFormat
    {
        public OrCADFormat(string fileText)
        {
            int perv = fileText.IndexOf("*NET");
            int end = fileText.IndexOf("*END") - perv;

            string clearText = fileText.Substring(perv, end).Replace('\r', ' ').Replace('\n', ' ');
            RemoveSpaces(ref clearText);
            RemoveDoubleNET(ref clearText);
            clearText = clearText.TrimEnd(' ');

            string[] splitClearText = clearText.Split('*');

            _hubsWithAllElement = new string[splitClearText.Length - 1][];

            for (int i = 1; i < splitClearText.Length; i++)
            {
                string[] splitLine = splitClearText[i].Split(' ');

                _hubsWithAllElement[i - 1] = new string[splitLine.Length];

                for (int j = 0; j < splitLine.Length; j++)
                {
                    _hubsWithAllElement[i - 1][j] = splitLine[j].Split('.')[0];
                }
            }
        }

        //задача этого метода заменить все двойные НЕТ на здвёздочки
        private void RemoveDoubleNET(ref string txt, int indexFind = 0)
        {
            int fourQ = 0;
            for (int i = indexFind, j = 0; j < 4; i++)
            {
                if (txt[i] == '\"')
                {
                    j++;
                    fourQ = i;
                }
            }

            string areaForDelete = txt.Substring(indexFind, fourQ - indexFind + 2);

            txt = txt.Replace(areaForDelete, "*");

            if (txt.Contains("*NET"))
                this.RemoveDoubleNET(ref txt, txt.IndexOf("*NET") - 1);
        }
    }

    class CL_90Format : SingleFormat
    {
        public CL_90Format(string fileText)
        {
            string clearText = fileText.Replace('\r', ' ').Replace('\n', ' ').Replace(',', ' ');
            RemoveSpaces(ref clearText);

            string[] splitMass = clearText.Split(';');

            _hubsWithAllElement = new string[splitMass.Length - 1][];            

            for (int i = 0; i < splitMass.Length -1; i++)
            {
                splitMass[i] = splitMass[i].TrimStart(' ');
                string[] hub = splitMass[i].Split(' ');

                _hubsWithAllElement[i] = new string[hub.Length -1];

                for (int j = 0; j < hub.Length - 1; j++)
                {
                    string item = hub[j + 1].Split('(')[0];

                    if (item.Contains('\"'))
                    {
                        item = item.Split('\"')[0];
                    }

                    _hubsWithAllElement[i][j] = item;
                }
            }
        }
    }
}
