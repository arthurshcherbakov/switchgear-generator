﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ZedGraph;
using testFacotConect.Model;

namespace testFacotConect
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class WindowAnal : Window
    {
        SchemeAnalyzer schemeAnalyzer;

        public FileReader FFFF;
        public WindowAnal()
        {

            
            InitializeComponent();


        }

        public void doit() 
        {


            //string path = @"C:\Program Files\Orcad\Layout_Plus\Samples\demo\EX_GUI.ASC";
            ////string path = @"C:\Program Files\Orcad\Layout_Plus\Samples\demo\SMRTROU2123.ASC";
            ////string path = @"C:\Program Files\Orcad\Layout_Plus\Samples\democ\DEMOC2123.ASC";

            ////string path = @"D:\носкова\К_выдаче_090402\allegro_1.NET";
            ////string path = @"D:\носкова\К_выдаче_090402\allegro_3.NET";
            ////string path = @"D:\носкова\К_выдаче_090402\shema6_cl90.NET";
            /////string path = @"D:\носкова\К_выдаче_090402\SCHEMS\BENCH_allegro.NET";
            //FileReader asd = new FileReader(path);




            schemeAnalyzer = new SchemeAnalyzer(FFFF);



            ZedCountConnect.GraphPane.CurveList.Clear();


            List<double> Xmas = new List<double>();
            List<double> Ymas = new List<double>();
            List<string> namesD = new List<string>();


            for (int i = 0; i < schemeAnalyzer.Cheme.CountConnetionStat.Histogram.Count(); i++)
            {
                if (schemeAnalyzer.Cheme.CountConnetionStat.Histogram[i][1] == 0)
                    continue;

                Xmas.Add(schemeAnalyzer.Cheme.CountConnetionStat.Histogram[i][0]);
                Ymas.Add(schemeAnalyzer.Cheme.CountConnetionStat.Histogram[i][1]);
                namesD.Add(Convert.ToString(Xmas.Last()));
            }

            double[] XmasD = Xmas.ToArray();
            double[] YmasD = Ymas.ToArray();
            string[] name = namesD.ToArray();

            ZedCountConnect.GraphPane.AddBar("", null, YmasD, System.Drawing.Color.Purple);
            ZedCountConnect.GraphPane.XAxis.Scale.MajorStep = 1.0;
            ZedCountConnect.GraphPane.XAxis.MajorTic.IsBetweenLabels = true;
            ZedCountConnect.GraphPane.XAxis.Type = AxisType.Text;
            ZedCountConnect.GraphPane.XAxis.Scale.TextLabels = name;
            ZedCountConnect.GraphPane.Border.IsVisible = false;

            ZedCountConnect.GraphPane.XAxis.Title.Text = "Варианты количества связей";
            ZedCountConnect.GraphPane.YAxis.Title.Text = "Количество вариатов";

            ZedCountConnect.GraphPane.Title.Text = "количество связей";
            //-------------------
            ZedFactConnect.GraphPane.CurveList.Clear();

            List<double> XmasF = new List<double>();
            List<double> YmasF = new List<double>();
            List<string> namesDF = new List<string>();


            for (int i = 0; i < schemeAnalyzer.Cheme.FactConnetionStat.Histogram.Count(); i++)
            {
                if (schemeAnalyzer.Cheme.FactConnetionStat.Histogram[i][1] == 0)
                    continue;

                XmasF.Add(schemeAnalyzer.Cheme.FactConnetionStat.Histogram[i][0]);
                YmasF.Add(schemeAnalyzer.Cheme.FactConnetionStat.Histogram[i][1]);
                namesDF.Add(Convert.ToString(XmasF.Last()));
            }

            double[] XmasDF = XmasF.ToArray();
            double[] YmasDF = YmasF.ToArray();
            string[] nameF = namesDF.ToArray();


            double[] TESTY = new double[YmasDF.Length];


            //ошибка
            //TESTY[5] = 36.0;



            // столбцы накладываются один на другой
            // всегда в одинаковой последовательности:
            ZedFactConnect.GraphPane.BarSettings.Type = BarType.Overlay;


            //ZedFactConnect.PointValueEvent+= new ZedGraphControl.PointValueHandler (zedGraph_PointValueEvent);
            //ZedFactConnect.PointValueEvent += testpredicat;

            // ZedFactConnect.GraphPane.BarSettings.ClusterScaleWidth

            //ZedFactConnect.GraphPane.BarSettings.ClusterScaleWidthAuto = false;
            //ZedFactConnect.GraphPane.BarSettings.Base = 

            /////////-----------------
            //ZedFactConnect.GraphPane.BarSettings.MinBarGap = 40;
            //ZedFactConnect.GraphPane.XAxis.Scale.Min = 5;
            //ZedFactConnect.GraphPane.XAxis.Scale.Max = 2;
            ////////////-------------

            //ZedFactConnect.GraphPane.AddBar("", null, TESTY, System.Drawing.Color.Yellow);
            ZedFactConnect.GraphPane.AddBar("", null, YmasDF, System.Drawing.Color.Purple);

            ZedFactConnect.GraphPane.XAxis.Scale.MajorStep = 1.0;

            ZedFactConnect.GraphPane.XAxis.MajorTic.IsBetweenLabels = true;

            ZedFactConnect.GraphPane.XAxis.Type = AxisType.Text;
            ZedFactConnect.GraphPane.XAxis.Scale.TextLabels = nameF;
            ZedFactConnect.GraphPane.Border.IsVisible = false;

            ZedFactConnect.GraphPane.XAxis.Title.Text = "Варианты фактов связи";
            ZedFactConnect.GraphPane.YAxis.Title.Text = "Количество вариатов";


            ZedFactConnect.GraphPane.Title.Text = "факты связей";
            //----------------------

            ZedCCFC.GraphPane.CurveList.Clear();

            List<double> masY = new List<double>();
            List<string> label = new List<string>();


            for (int i = 0; i < schemeAnalyzer.Cheme.HistogramCountAndFactConnetion.Count(); i++)
            {
                if (schemeAnalyzer.Cheme.HistogramCountAndFactConnetion[i][2] == 0)
                    continue;

                masY.Add(schemeAnalyzer.Cheme.HistogramCountAndFactConnetion[i][2]);
                label.Add(Convert.ToString(schemeAnalyzer.Cheme.HistogramCountAndFactConnetion[i][0])
                    + "|" + Convert.ToString(schemeAnalyzer.Cheme.HistogramCountAndFactConnetion[i][1]));
            }

            double[] masYF = masY.ToArray();
            string[] labelF = label.ToArray();


            /////*-----------

            // ZedCCFC.GraphPane.BarSettings.ClusterScaleWidthAuto = false;
            /////-****************

            PointPairList list = new PointPairList();
            PointPairList listFirst = new PointPairList();


            for (int i = 0; i < masYF.Length; i++)
            {
                list.Add(i, masYF[i]);
                listFirst.Add(i, 0);
            }
            ZedCCFC.PointValueEvent += new ZedGraphControl.PointValueHandler(zedGraph_PointValueEvent);
            ZedCCFC.IsShowPointValues = true;
            ZedFactConnect.IsShowPointValues = true;
            ZedCountConnect.IsShowPointValues = true;

            ZedCCFC.GraphPane.AddBar("asd", listFirst, System.Drawing.Color.Orchid);
            ZedCCFC.GraphPane.AddBar("asd", list, System.Drawing.Color.Tomato);

            //ZedCCFC.GraphPane.AddCurve("asd", list, System.Drawing.Color.Tomato);
            //ZedCCFC.GraphPane.AddBar("", null, masYF, System.Drawing.Color.Purple);


            // всегда в одинаковой последовательности:
            ZedCCFC.GraphPane.BarSettings.Type = BarType.Overlay;
            ZedCCFC.GraphPane.XAxis.Scale.MajorStep = 1.0;

            ZedCCFC.GraphPane.XAxis.MajorTic.IsBetweenLabels = true;


            ZedCCFC.GraphPane.XAxis.Type = AxisType.Text;
            ZedCCFC.GraphPane.XAxis.Scale.TextLabels = labelF;
            ZedCCFC.GraphPane.Border.IsVisible = false;

            ZedCCFC.GraphPane.XAxis.Title.Text = "количество фактов связи | количество связи";


            ZedCountConnect.AxisChange();
            ZedCountConnect.Invalidate();

            ZedFactConnect.AxisChange();
            ZedFactConnect.Invalidate();



            ZedCCFC.AxisChange();
            ZedCCFC.Invalidate();
        } 


       
        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {

            //assd.CurveList[0].Points[0].Y = 20;
            ZedCountConnect.AxisChange();
            ZedCountConnect.Invalidate();

            //foreach (var item in gfds.Cheme.FactConnetionStat.Histogram)
            //{
            //    //FactConect.Add(new Point(item[0], item[1]));

            //    qqqqqq.Add(new int[] { item[0], item[1] });
            //}

            //foreach (var item in gfds.Cheme.CountConnetionStat.Histogram)
            //{
            //    CounConect.Add(new Point(item[0], item[1]));
            //}

            ////ColumnSeries.ItemsSource = FactConect;
            //ColumnSeries.ItemsSource = qqqqqq;
            //CC.ItemsSource = CounConect;

            //ZedFactConnect.GraphPane.CurveList[0].Label;
        }


        string zedGraph_PointValueEvent(ZedGraphControl sender,
           GraphPane pane,
           CurveItem curve,
           int iPt)
        {
            // Получим точку, около которой находимся
            PointPair point = curve[iPt];

            //перекрашивание
            sender.GraphPane.CurveList[0].Points[3].Y = 23;

            for (int i = 0; i < sender.GraphPane.CurveList[0].Points.Count; i++)
            {
                sender.GraphPane.CurveList[0].Points[i].Y = 0;
            }

            sender.GraphPane.CurveList[0].Points[iPt].Y = sender.GraphPane.CurveList[1].Points[iPt].Y;

            string title = sender.GraphPane.XAxis.Scale.TextLabels[iPt];

            string result = string.Format("X: {0}\nY: {1:F3}", title, point.Y);

            // Сформируем строку
            //string result = string.Format("X: {0:F3}\nY: {1:F3}", point.X, point.Y);

            sender.AxisChange();
            sender.Invalidate();

            param.Content = result;
            return result;
        }








        //1 область столбца 
        //2 можно ли изменить цвет одного столбца

        //private void ZedFactConnect_MouseClick(object sender, System.Windows.Forms.MouseEventArgs e)
        //{
        //    double x, y;



        //    ZedFactConnect.GraphPane.ReverseTransform(e.Location, out x, out y);
        //    string text = string.Format("X: {0};    Y: {1}", x, y);

        //    ZedCountConnect.AxisChange();
        //    ZedCountConnect.Invalidate();
        //}




    }
}
