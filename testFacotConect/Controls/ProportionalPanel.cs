﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace testFacotConect.Controls
{
    public class ProportionalPanel : Panel
    {
        public string NameBinding
        {
            get { return (string)GetValue(NameBindingProperty); }
            set { SetValue(NameBindingProperty, value); }
        }

        public static DependencyProperty NameBindingProperty = 
            DependencyProperty.Register("NameBinding", 
            typeof(string), 
            typeof(ProportionalPanel), new PropertyMetadata("Default"));  

        public ItemsControl ItemsControlData
        {
            get { return (ItemsControl)GetValue(ItemsControlProperty); }
            set { SetValue(ItemsControlProperty, value); }
        }

        public static readonly DependencyProperty ItemsControlProperty =
                    DependencyProperty.Register("ItemsControlData", typeof(ItemsControl),
                        typeof(ProportionalPanel), new FrameworkPropertyMetadata(null,
                FrameworkPropertyMetadataOptions.AffectsArrange |
                FrameworkPropertyMetadataOptions.AffectsMeasure | 
                FrameworkPropertyMetadataOptions.AffectsRender));

        ////ProportionHeight
        //public static double GetProportionHeight(DependencyObject obj)
        //{
        //    return (double)obj.GetValue(ProportionPropertyHeight);
        //}

        //public static void SetProportionHeight(DependencyObject obj, double value)
        //{
        //    obj.SetValue(ProportionProperty, value);
        //}

        //public static readonly DependencyProperty ProportionPropertyHeight =
        //    DependencyProperty.RegisterAttached("ProportionHeight", typeof(double), typeof(ProportionalPanel),
        //    new FrameworkPropertyMetadata(1.0, FrameworkPropertyMetadataOptions.AffectsArrange |
        //        FrameworkPropertyMetadataOptions.AffectsMeasure |
        //        FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.AffectsParentArrange | FrameworkPropertyMetadataOptions.AffectsParentMeasure));
        ////end ProportionHeight

        //ProportionWidth
        public static double GetProportion(DependencyObject obj)
        {
            return (double)obj.GetValue(ProportionProperty);
        }

        public static void SetProportion(DependencyObject obj, double value)
        {
            obj.SetValue(ProportionProperty, value);
        }

        public static readonly DependencyProperty ProportionProperty =
            DependencyProperty.RegisterAttached("Proportion", typeof(double), typeof(ProportionalPanel),
            new FrameworkPropertyMetadata(1.0, 
                FrameworkPropertyMetadataOptions.AffectsArrange |
                FrameworkPropertyMetadataOptions.AffectsMeasure |
                FrameworkPropertyMetadataOptions.AffectsRender | 
                FrameworkPropertyMetadataOptions.AffectsParentArrange |
                FrameworkPropertyMetadataOptions.AffectsParentMeasure ));

        //end ProportionWidth
        public Orientation Orientation
        {
            get { return (Orientation)GetValue(OrientationProperty); }
            set { SetValue(OrientationProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Orientation.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty OrientationProperty =
            DependencyProperty.Register("Orientation", typeof(Orientation), typeof(ProportionalPanel),
            new FrameworkPropertyMetadata(Orientation.Vertical,
                FrameworkPropertyMetadataOptions.AffectsArrange | FrameworkPropertyMetadataOptions.AffectsMeasure | FrameworkPropertyMetadataOptions.AffectsRender));

        public void doit() 
        {
            if (this.ItemsControlData != null && this.NameBinding != "Default")
            {
                foreach (var item in this.ItemsControlData.Items)
                {
                    UIElement uiElement =
                        (UIElement)this.ItemsControlData.ItemContainerGenerator.ContainerFromItem(item);

                    Type myType = typeof(ItemHistogram);
                    PropertyInfo pinfo = myType.GetProperty(NameBinding);
                    double valueProp = (double)pinfo.GetValue((ItemHistogram)item);

                    uiElement.SetValue(ProportionalPanel.ProportionProperty, valueProp);
                }
            }

            double childWidth = 0.0;
            double childHeight = 0.0;
            double lastChildX = 0.0;
            double lastChildY = 0.0;
            double totalProportion = 0.0;

            foreach (UIElement child in this.Children)
            {
                totalProportion += GetProportion(child);
            }

            foreach (UIElement child in this.Children)
            {
                if (Orientation == Orientation.Horizontal)
                {
                    double weweqw = GetProportion(child);
                    double sdwds = child.Opacity;
                    childWidth = (double)mysize.Width * (GetProportion(child) / totalProportion);
                    childHeight = mysize.Height;

                }
                if (Double.IsNaN(childWidth))
                    childWidth = 0.0;
                if (Double.IsNaN(childHeight))
                    childHeight = 0.0;

                child.Arrange(new Rect(lastChildX, lastChildY, childWidth, childHeight));


                if (Orientation == Orientation.Horizontal)
                {
                    lastChildX += childWidth;
                }
            }

        }

        Size mysize;

        protected override Size ArrangeOverride(Size arrangeSize)
        {
            if (this.ItemsControlData != null && this.NameBinding != "Default")
            {
                foreach (var item in this.ItemsControlData.Items)
                {
                    UIElement uiElement =
                        (UIElement)this.ItemsControlData.ItemContainerGenerator.ContainerFromItem(item);

                    Type myType = typeof(ItemHistogram);
                    PropertyInfo pinfo = myType.GetProperty(NameBinding);
                    double valueProp = (double)pinfo.GetValue((ItemHistogram)item);

                    uiElement.SetValue(ProportionalPanel.ProportionProperty, valueProp);
                }
            }
           
            double childWidth = 0.0;
            double childHeight = 0.0;
            double lastChildX = 0.0;
            double lastChildY = 0.0;
            double totalProportion = 0.0;

            foreach (UIElement child in this.Children)
            {
                totalProportion += GetProportion(child);
                

            }
            foreach (UIElement child in this.Children)
            {
                if (Orientation == Orientation.Horizontal)
                {
                    double weweqw = GetProportion(child);
                    double sdwds = child.Opacity;
                    childWidth = (double)arrangeSize.Width * (GetProportion(child) / totalProportion);
                    childHeight = arrangeSize.Height;

                    double erew = GetProportion(child);
                }

                //if (Orientation == Orientation.Vertical)
                //{
                //    childWidth = arrangeSize.Width;
                //    childHeight = arrangeSize.Height * (GetProportion(child) / totalProportion);
                //}

                if (Double.IsNaN(childWidth))
                    childWidth = 0.0;
                if (Double.IsNaN(childHeight))
                    childHeight = 0.0;

                child.Arrange(new Rect(lastChildX, lastChildY, childWidth, childHeight));


                if (Orientation == Orientation.Horizontal)
                {
                    lastChildX += childWidth;
                }
                //if (Orientation == Orientation.Vertical)
                //{
                //    lastChildY += childHeight;
                //}
            }
            return arrangeSize;
        }

        protected override Size MeasureOverride(Size constraint)
        {
            if (this.ItemsControlData != null && this.NameBinding != "Default")
            {
                foreach (var item in this.ItemsControlData.Items)
                {
                    UIElement uiElement =
                        (UIElement)this.ItemsControlData.ItemContainerGenerator.ContainerFromItem(item);
                    if (uiElement == null)
                    {
                        break;
                    }

                    Type myType = typeof(ItemHistogram);
                    PropertyInfo pinfo = myType.GetProperty(NameBinding);
                    double valueProp = (double)pinfo.GetValue((ItemHistogram)item);

                    uiElement.SetValue(ProportionalPanel.ProportionProperty, valueProp);
                }
            }

            mysize = constraint;

            double childWidth = 0.0;
            double childHeight = 0.0;
            double maxChildHeight = 0.0;
            double maxChildWidth = 0.0;

            double totalProportion = 0.0;
            foreach (UIElement child in this.Children)
            {
                totalProportion += GetProportion(child);
            }
            foreach (UIElement child in this.Children)
            {
                if (Orientation == Orientation.Horizontal)
                {
                    childWidth = constraint.Width * (GetProportion(child) / totalProportion);
                    childHeight = constraint.Height;


                }

                //if (Orientation == Orientation.Vertical)
                //{
                //    childWidth = constraint.Width;
                //    childHeight = constraint.Height * (GetProportion(child) / totalProportion);
                //}

                if (Double.IsNaN(childWidth))
                    childWidth = 0.0;

                if (Double.IsNaN(childHeight))
                    childHeight = 0.0;

                child.Measure(new Size(childWidth, childHeight));

                maxChildHeight = Math.Max(child.DesiredSize.Height, maxChildHeight);
                maxChildWidth = Math.Max(child.DesiredSize.Width, maxChildWidth);
            }

            Size controlSize = new Size();

            if (Orientation == Orientation.Horizontal)
            {
                controlSize = new Size(constraint.Width, maxChildHeight);
            }
            //if (Orientation == Orientation.Vertical)
            //{
            //    controlSize = new Size(maxChildWidth, constraint.Height);
            //}
            return controlSize;
        }
    }

}
