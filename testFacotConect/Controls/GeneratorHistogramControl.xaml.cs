﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


//динамическое добовление столбца

//возможность выдилить столбец (обозначить изменением цвета чево-либо)

//подумать как контролл должен взаимодейстовать с моделью () 

//подумать о размерных характерристиках контролла 
//1 динамичное изменение ширины
//2

// сделать систему баланса как в игре "симулятор разрабочика игр"

//сделать кнопку запрещающую изменять соотношения для конкретного столбца

//что из себя представляет столбец 
//1 границы 
//2 высота это значение, в частях


//как должно выглядеть количество связи
//1 нужно сделать специальный режим настройки. 
//это будут столбцы поверх столбцов фактов связи

//показатели области столбца гистограммы будут отображаться сверху и снизу 
//число рисовать с правого края столбца

namespace testFacotConect.Controls
{

    public class Element
    {
        #region Fields

        private bool isStretching = false;

        private IInputElement inputElement = null;
        private double x, y = 0;
        private int zIndex = 0;
        #endregion

        #region Constructor
        public Element() { }
        #endregion

        #region Properties
        public IInputElement InputElement
        {
            get { return this.inputElement; }
            set
            {
                this.inputElement = value;
                this.isStretching = false;
            }
        }
        public double X
        {
            get { return this.x; }
            set { this.x = value; }
        }
        public double Y
        {
            get { return this.y; }
            set { this.y = value; }
        }
        public int ZIndex
        {
            get { return this.zIndex; }
            set { this.zIndex = value; }
        }

        public bool IsStretching
        {
            get { return this.isStretching; }
            set
            {
                this.isStretching = value;
            }
        }

        #endregion
    }


    /// <summary>
    /// Логика взаимодействия для GeneratorHistogramControl.xaml
    /// </summary>
    public partial class GeneratorHistogramView : UserControl
    {
        GeneratorHistogramViewModel _viewModel;
        public RelayCommand upmous { get; set; }
        private Element current = new Element();

        //разница между выстой кнопки и бордера который хватает пользователь
        double heightBethBorderAndButton = 15;

        public GeneratorHistogramView()
        {
            InitializeComponent();

            //_viewModel = new GeneratorHistogramViewModel();
            upmous = new RelayCommand(umvoidmouse);
            //NameComand = new RelayCommand(umvoidmouse);

            _viewModel = (GeneratorHistogramViewModel)rootGrid.DataContext;

            _viewModel.FactConnectHistogramZIdex = 15;
            _viewModel.CountConnectHistogramZIdex = 10;
                     
            

        }


        public void umvoidmouse()
        {
           
        }




        //com
        public RelayCommand NameComand
        {
            get { return (RelayCommand)GetValue(NameComandDP); }
            set { SetValue(NameComandDP, value); }
        }

        public static readonly DependencyProperty NameComandDP =
            DependencyProperty.Register("NameComand", typeof(RelayCommand),
            typeof(GeneratorHistogramView),
            new PropertyMetadata(null));
        //end com
        //1
        public ObservableCollection<ItemHistogram> ItemsFactOfConnection
        {
            get { return (ObservableCollection<ItemHistogram>)GetValue(ItemsFactOfConnectionProperty); }
            set { SetValue(ItemsFactOfConnectionProperty, value); }
        }

        public static readonly DependencyProperty ItemsFactOfConnectionProperty =
                    DependencyProperty.Register("ItemsFactOfConnection", typeof(ObservableCollection<ItemHistogram>),
                        typeof(GeneratorHistogramView), new PropertyMetadata(null, OnIconInfosSet));

        private static void OnIconInfosSet(DependencyObject d,
            DependencyPropertyChangedEventArgs e)
        {
            ((GeneratorHistogramView)d)._viewModel.ItemsFactOfConnection = e.NewValue as ObservableCollection<ItemHistogram>;
        }
        //1 end

        //2
        public ObservableCollection<ItemHistogram> ItemsCountConnection
        {
            get { return (ObservableCollection<ItemHistogram>)GetValue(ItemsCountConnectionProperty); }
            set { SetValue(ItemsCountConnectionProperty, value); }
        }

        public static readonly DependencyProperty ItemsCountConnectionProperty =
                    DependencyProperty.Register("ItemsCountConnection", typeof(ObservableCollection<ItemHistogram>),
                        typeof(GeneratorHistogramView), new PropertyMetadata(null, OnIconInfosSetTwo));

        private static void OnIconInfosSetTwo(DependencyObject d,
            DependencyPropertyChangedEventArgs e)
        {
            ((GeneratorHistogramView)d)._viewModel.ItemsCountConnection = e.NewValue as ObservableCollection<ItemHistogram>;
        }
        //2 end

        //bool
        public bool EnableParam
        {
            get { return (bool)GetValue(EnableParamProperty); }
            set { SetValue(EnableParamProperty, value); }
        }

        public static readonly DependencyProperty EnableParamProperty =
            DependencyProperty.Register("EnableParam", typeof(bool),
            typeof(GeneratorHistogramView),
            new PropertyMetadata(update));

        private static void update(DependencyObject d,
            DependencyPropertyChangedEventArgs e)
        {
            // менять слои 
            // менять енабле 
            //снимать выдиление с элмента (которое ещё не сделано)

            bool paramMode = ((bool)e.NewValue);

            if (paramMode)
            {
                ((GeneratorHistogramView)d)._viewModel.FactConnectHistogramZIdex = 15;
                ((GeneratorHistogramView)d)._viewModel.CountConnectHistogramZIdex = 10;
            }
            else
            {
                ((GeneratorHistogramView)d)._viewModel.FactConnectHistogramZIdex = 10;
                ((GeneratorHistogramView)d)._viewModel.CountConnectHistogramZIdex = 15;
            }
            ((GeneratorHistogramView)d)._viewModel.FactConnectEnableParam = paramMode;
            ((GeneratorHistogramView)d)._viewModel.CountConnectEnableParam = !paramMode;


        }
        //bool end

        //selected item
        public ItemHistogram SelectedItem
        {
            get { return (ItemHistogram)GetValue(SelectedItemHistogram); }
            set { SetValue(SelectedItemHistogram, value); }
        }

        public static readonly DependencyProperty SelectedItemHistogram =
                    DependencyProperty.Register("SelectedItem", typeof(ItemHistogram),
                        typeof(GeneratorHistogramView), new PropertyMetadata(SelectedItemHistogramUpdate));

        private static void SelectedItemHistogramUpdate(DependencyObject d,
            DependencyPropertyChangedEventArgs e)
        {
            //((GeneratorHistogramView)d)._viewModel.SelectedItem = 
        }
        //selected item end

        //public bool xui
        //{
        //    get { return (bool)GetValue(xuiProperty); }
        //    set { SetValue(xuiProperty, value); }
        //}

        //public static readonly DependencyProperty xuiProperty =
        //    DependencyProperty.Register("xui", typeof(bool),
        //    typeof(GeneratorHistogramView),
        //    new PropertyMetadata(false));


        
        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);
            this.current.X = Mouse.GetPosition(this.rootGrid).X;
            this.current.Y = Mouse.GetPosition(this.rootGrid).Y;

            if (this.current.InputElement != null)
                this.current.InputElement.CaptureMouse();


        }
        protected override void OnMouseLeftButtonUp(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonUp(e);

            if (this.current.InputElement != null)
                this.current.InputElement.ReleaseMouseCapture();

            this.Cursor = Cursors.Arrow;
        }





        private void Canvas_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed &&
                 current.InputElement != null)
            {
                //increment z-Order and pass it to the current element, 
                //so that it stays on top of all other elements
                //((Border)this.current.InputElement).SetValue(Canvas.ZIndexProperty, this.current.ZIndex++);

                //if (this.current.IsDragging)
                //    Drag(sender);

                if (this.current.IsStretching) 
                {
                    Stretch(sender);
                    NameComand.Execute();
                }                    
            }
        }

        private void border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //capture the last highest z index before pointing to new current element
            //int newZIndex = (int)((Border)sender).GetValue(Canvas.ZIndexProperty);
            //this.current.ZIndex = newZIndex > this.current.ZIndex ? newZIndex : this.current.ZIndex;


            //capture the new current element
            //this.current.InputElement = (IInputElement)sender;
            //this.current.InputElement = (IInputElement)sender;

            if (this.current.InputElement != null)
            {
                this.current.IsStretching = true;
            }

        }
        //private void border_MouseLeave(object sender, MouseEventArgs e)
        //{
        //    if (e.LeftButton == MouseButtonState.Pressed)
        //        return;

        //    //// get coordinates
        //    //Border border = (Border)sender;
        //    //var rightLimit = border.ActualWidth - border.Padding.Right;
        //    //var bottomLimit = border.ActualHeight - border.Padding.Bottom;


        //    //// figure out stretching directions - only to Right, Bottom 
        //    //bool stretchRight = (x >= rightLimit && x < border.ActualWidth) ? true : false;
        //    //bool stretchBottom = (y >= bottomLimit && y < border.ActualHeight) ? true : false;

        //    var X_Mouse = Mouse.GetPosition((IInputElement)sender).X;
        //    var Y_Mouse = Mouse.GetPosition((IInputElement)sender).Y;



        //    const double GAP = 15;

        //    // update current element
        //    // this.current.InputElement = (IInputElement)sender;
        //    this.current.X = X_Mouse;
        //    this.current.Y = Y_Mouse;
        //    //this.current.IsStretching = true;


        //    if (Y_Mouse >= 0 && Y_Mouse <= GAP
        //        && X_Mouse >= 0 && X_Mouse <= ((Border)sender).ActualWidth)
        //    {
        //        this.Cursor = Cursors.SizeNS;
        //        return;
        //    }
        //    else
        //    {
        //        this.Cursor = Cursors.Arrow;
        //        this.current.IsStretching = false;
        //        return;
        //    }






        //}
        //private void border_MouseEnter(object sender, MouseEventArgs e)
        //{


        //    //Border border = (Border)sender;



        //    //var TOPLimit = border.ActualHeight - border.Padding.Top;

        //    //var x = Mouse.GetPosition((IInputElement)sender).X;
        //    //var y = Mouse.GetPosition((IInputElement)sender).Y;

        //    //if (y > border.Padding.Top)
        //    //    this.Cursor = Cursors.Arrow;
        //}

        //изменение высоты столбца    
        private void Stretch(object sender)
        {

            // Retrieve the current position of the mouse.
            var mousePosX = Mouse.GetPosition((IInputElement)sender).X;
            var mousePosY = Mouse.GetPosition((IInputElement)sender).Y;


            //get coordinates
            Border border = (Border)this.current.InputElement;
            var xDiff = mousePosX - this.current.X;
            var yDiff = mousePosY - this.current.Y;
            var width = ((Border)this.current.InputElement).Width;
            var heigth = ((Border)this.current.InputElement).Height;


            //make sure not to resize to negative width or heigth
            xDiff = (border.Width + xDiff) > border.MinWidth ? xDiff : border.MinWidth;
            yDiff = (border.Height + yDiff) > border.MinHeight ? yDiff : border.MinHeight;


            if (this.Cursor == Cursors.SizeNS)
            {
                if (((Border)this.current.InputElement).Height - yDiff > heightBethBorderAndButton)
                {
                    ((Border)this.current.InputElement).Height -= yDiff;    
                }

                
            }

            this.current.Y = mousePosY;
        }

        private void border3_MouseMove(object sender, MouseEventArgs e)
        {
            if ((Border)sender != ((Border)this.current.InputElement))
            {
                return;
            }

            var X_Mouse = Mouse.GetPosition((IInputElement)sender).X;
            var Y_Mouse = Mouse.GetPosition((IInputElement)sender).Y;

            if (e.LeftButton == MouseButtonState.Pressed)
                return;


            const double GAP = 15;

            // update current element
            // this.current.InputElement = (IInputElement)sender;
            this.current.X = X_Mouse;
            this.current.Y = Y_Mouse;
            //this.current.IsStretching = true;


            if (Y_Mouse >= 0 && Y_Mouse <= GAP
                && X_Mouse >= 0 && X_Mouse <= ((Border)sender).ActualWidth)
            {
                this.Cursor = Cursors.SizeNS;
                return;
            }
            else
            {
                this.Cursor = Cursors.Arrow;
                this.current.IsStretching = false;
                return;
            }

        }

        private void border3_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            this.current.IsStretching = false;
        }




        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.current.InputElement = ((Border)((Grid)sender).Parent);
        }

        //клик по кнопке для выбора 
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (this.current.InputElement != null)
            {
                ((Border)this.current.InputElement).Padding = new Thickness(0, 0, 0, 0);
            }
            

            this.current.InputElement = ((Border)((Button)sender).Parent);

            this.SelectedItem = (ItemHistogram)((Button)sender).Tag;

            ((Border)this.current.InputElement).Padding = new Thickness(0, heightBethBorderAndButton, 0, 0);

            //if (NameComand == null)
            //{
            //    NameComand = new RelayCommand(umvoidmouse);

            //}



            //double sadw = ProportionalPanel.GetProportion(((Border)((Button)sender).Parent));

            //Border sdsdwdwaw = ((Border)((Button)sender).Parent);
            //double wdwdw = sdsdwdwaw.Opacity;

            //UIElement sdwds = (UIElement)sdsdwdwaw.Parent;



            //foreach (var item in qwertyt.Items)
            //{
            //    UIElement uiElement =
            //        (UIElement)qwertyt.ItemContainerGenerator.ContainerFromItem(item);
            //    double awds = (double)uiElement.GetValue(ProportionalPanel.ProportionProperty);
            //}




            //if (this.SelectedItem !=null)
            //{
            //    UIElement buf = (UIElement)qwertyt.ItemContainerGenerator.ContainerFromItem(this.SelectedItem);
            //    buf.SetValue(ProportionalPanel.ProportionProperty, 1.0);
            //}

            

            //UIElement uiElement = (UIElement)qwertyt.ItemContainerGenerator.ContainerFromItem(this.SelectedItem);
            //uiElement.SetValue(ProportionalPanel.ProportionProperty, 2.0);










            //ItemFactOfConnection buf = (ItemFactOfConnection)((Button)sender).Tag;
            //  string sdwd=  uiElement.; 

        }




    }

}





