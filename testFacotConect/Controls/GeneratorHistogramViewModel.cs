﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace testFacotConect.Controls
{
    internal class GeneratorHistogramViewModel : BindableBase
    {
        private ObservableCollection<ItemHistogram> _itemsFactOfConnection;
        private bool _factConnectEnableParam;
        private bool _countConnectEnableParam;
        private byte _factConnectHistogramZIdex;
        private byte _countConnectHistogramZIdex;
        private ObservableCollection<ItemHistogram> _itemsCountConnection;
        private ItemHistogram _selectedItem;
        
        public byte FactConnectHistogramZIdex
        {
            get { return _factConnectHistogramZIdex; }
            set { SetProperty(ref _factConnectHistogramZIdex, value); }
        }

        public ItemHistogram SelectedItem
        {
            get { return _selectedItem; }
            set { SetProperty(ref _selectedItem, value); }
        }


        public byte CountConnectHistogramZIdex
        {
            get { return _countConnectHistogramZIdex; }
            set { SetProperty(ref _countConnectHistogramZIdex, value); }
        }

        public bool FactConnectEnableParam 
        {
            get { return _factConnectEnableParam; }
            set { SetProperty(ref _factConnectEnableParam, value); }
        }

        public bool CountConnectEnableParam
        {
            get { return _countConnectEnableParam; }
            set { SetProperty(ref _countConnectEnableParam, value); }          
        }

        public ObservableCollection<ItemHistogram> ItemsFactOfConnection
        {
            get { return _itemsFactOfConnection; }
            set { SetProperty(ref _itemsFactOfConnection, value); }
        }

        public ObservableCollection<ItemHistogram> ItemsCountConnection
        {
            get { return _itemsCountConnection; }
            set { SetProperty(ref _itemsCountConnection, value); }
        }

        public GeneratorHistogramViewModel()
        {
            //NameComand = new RelayCommand(some);

            
        }



    }
}
