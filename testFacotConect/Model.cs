﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace testFacotConect
{
    //а если у каждого элемента будет свой лист из обьектов тогоже класса 
    //

    public class ItemHistogram : BindableBase
    {
        //должно быть значение указывающие на минимально возможную высоту, 
        //в случае с количеством фактов связи должна сущестовавать 
        //минимальная зарание вычисленная величина которая не будет 
        //позволять задать количество связей меньшее чем количестов фактов связи, так как это противоречит здравомосмыслу

        //const int MAX_OF_HEIGHT = 100;
        //const int MIN_OF_HEIGHT = 1;

        #region Fields
        int start;
        int end;

        //количство элементов для ItemsFactOfConnection
        int countElement;
        //всё количество FC Для диапозона
        int _allFCForItem;
        //количество внутренних соединений
        int _internalFC;
        //количество внешних соединений
        int _externalFC;
        //КС диапозона, указыватется дополнительные соединения 
        int _countConnection;

        //свойсто служащие для отмены участия элемента расчёте процентного отношения 
        bool block = false;
        //свойсво нужное для обозначения может ли элемент хранить в себе лист
        bool insideList = false;
        double percentPart;
        double heightColumn;
        double widthProportional;

        GraphType.typegraph typeGraph;
        #endregion

        #region Properties
        public int СountConnection
        {
            get
            {
                return _countConnection;
            }
            set
            {
                _countConnection = value;
            }
        }


        public int AllFCForItem
        {
            get
            {
                return _allFCForItem;
            }
            set
            {
                _allFCForItem = value;
            }
        }
        public int InternalFC
        {
            get
            {
                return _internalFC;
            }
            set
            {
                _internalFC = value;
            }
        }
        public int ExternalFC
        {
            get
            {
                return _externalFC;
            }
            set
            {
                _externalFC = value;
            }
        }


        public int CountElement
        {
            get
            {
                return countElement;
            }
            set
            {
                countElement = value;
            }
        }

        public double WidthProportional
        {
            get
            {
                return widthProportional;
            }
            set
            {
                widthProportional = value;
            }
        }
        
        public bool InsideList
        {
            get
            {
                return insideList;
            }
            set
            {
                insideList = value;
            }
        }
        public ItemHistogram GetThis
        {
            get
            {
                return this;
            }
        }
        public int Start
        {
            get
            {
                return start;
            }
            set
            {
                //start = value;
                SetProperty(ref start, value);
            }
        }
        public int End
        {
            get
            {
                return end;
            }
            set
            {
                SetProperty(ref end, value);
                //end = value;
            }
        }
        public bool Block
        {
            get
            {
                return block;
            }
            set
            {
                block = value;
            }
        }
        public double PercentPart
        {
            get
            {
                return percentPart;
            }
            set
            {
                percentPart = value;
            }
        }
        public double HeightColumn
        {
            get
            {
                return heightColumn;
            }
            set
            {
                heightColumn = value;                              
            }
        }
        public GraphType.typegraph TypeGraph
        {
            get
            {
                return typeGraph;
            }
            set
            {
                typeGraph = value;
            }
        }
        #endregion

        
    }

    public class GraphType
    {
        public enum typegraph { one, two };
        public static int dosome()
        {
            return 0;
        }
    }

    public class cce
    {
        int start;
        int end;

    }

    //public class Allall : BindableBase
    //{
    //    public enum typegraph { one, two };

    //    ObservableCollection<ItemHistogram> _ffelist;
    //    //ObservableCollection<cce> ccelist;



    //    public ObservableCollection<ItemHistogram> Ffelist
    //    {
    //        get { return _ffelist; }
    //        set { SetProperty(ref _ffelist, value, "Ffelist"); }
    //    }

    //    public Allall(int start, int end)
    //    {
    //        Ffelist = new ObservableCollection<ItemHistogram>();
    //        Ffelist.Add(new ItemHistogram());


    //        Ffelist[0].End = 2;
    //        Ffelist.Add(new ItemHistogram());


    //    }

    //}
}
