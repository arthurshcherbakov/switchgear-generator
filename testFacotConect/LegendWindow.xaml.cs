﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;

namespace testFacotConect
{
    /// <summary>
    /// Логика взаимодействия для LegendWindow.xaml
    /// </summary>
    public partial class LegendWindow : Window
    {
        public bool ClosingLegend = true;
        public void SetContent(Color[] colorsLine, string[] coments)
        {
            ListLegend.Items.Clear();

            for (int i = 0; i < coments.Length; i++)
            {
                Grid content = new Grid();

                Point startP = new Point(10, 10);
                Point endP = new Point(60, 10);

                TextBlock comentText = new TextBlock();
                comentText.Text = coments[i];
                comentText.FontSize = 16;
                comentText.Margin = new Thickness(endP.X + 10, 0, 0, 0);

                Path myPath = new Path();
                myPath.Stroke = new SolidColorBrush(colorsLine[i]);
                myPath.StrokeThickness = 5;
                PathFigure PathLegend = new PathFigure();
                PathLegend.StartPoint = startP;
                PathLegend.Segments.Add(
                                        new LineSegment(endP, true));
                PathGeometry newPathGeometry = new PathGeometry();
                newPathGeometry.Figures.Add(PathLegend);
                myPath.Data = newPathGeometry;


                content.Children.Add(myPath);
                content.Children.Add(comentText);

                ListBoxItem itm = new ListBoxItem();
                itm.Height = 40;
                itm.Content = content;
                ListLegend.Items.Add(itm);
            }
        }

        public LegendWindow()
        {
            InitializeComponent();

            this.Closing += CancelClosing;
        }

        void CancelClosing(object sender, CancelEventArgs e)
        {
            e.Cancel = ClosingLegend;
            this.Visibility = Visibility.Hidden;
        }
    }
}
