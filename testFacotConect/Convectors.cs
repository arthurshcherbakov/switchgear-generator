﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace testFacotConect
{
    [ValueConversion(typeof(double), typeof(string))]
    public class ConvStringDouble : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {

            //string a = (string)value;
            //double b;
            //double.TryParse(a, out b);
            //return b;

            return value.ToString();

        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double result;
            if (Double.TryParse(value.ToString(), out result))
            {
                return result;
            }


            return DependencyProperty.UnsetValue;

        }
    }


    [ValueConversion(typeof(double), typeof(string))]
    public class ConvStringDoublePercent : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double par;
            double.TryParse((string)parameter, out par);
            double res = (double)value / (par / 100);

            //string a = (string)value;
            //double b;
            //double.TryParse(a, out b);
            //return b;

            return res.ToString("F");

        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double result;
            
                if (Double.TryParse(value.ToString(), out result))
                {
                    if (result > 0)
                    {
                        double par;
                        double.TryParse((string)parameter, out par);

                        return result * (par / 100);
                    }                    
                }
            return DependencyProperty.UnsetValue;
        }
    }
}
