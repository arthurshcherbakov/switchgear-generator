﻿using Microsoft.Win32;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using System.ComponentModel;




namespace testFacotConect
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class poslInerc : Window
    {
        public FileReader Allegro;
        //RereadingAllegro Allegro;
        CoherentIterative Matrix;
        LegendWindow Legend;

        int colorMode;
        public poslInerc()
        {
            InitializeComponent();
            ClearAndHidden();
            colorMode = 0;
            //управление закрытием легенды
            Legend = new LegendWindow();
            this.Closing += Closing_MainWindow;
            //для быстрых тестов




            //Allegro = new RereadingAllegro(@"D:\носкова\К_выдаче_090402\allegro_3.NET");
            //StepFirst();
            //Matrix = new CoherentIterative(Allegro.Contiguity);
            //Matrix.createMatrixR();
            //TextBoxOfBeforeQuality.Text = Convert.ToString(Matrix.QualityBefore);

            ////param
            //int factorSize = 100;
            //int[] ofSet = new int[] { 200, 100 };
            //int sizeSquare = 50;


            //CreateRmatrix(factorSize, ofSet, sizeSquare, colorMode);
            //StepSecond();
        }

        public CoherentIterative CoherentIterative
        {
            get
            {
                throw new System.NotImplementedException();
            }
            set
            {
            }
        }

        public LegendWindow LegendWindow
        {
            get
            {
                throw new System.NotImplementedException();
            }
            set
            {
            }
        }

        //public Allegro.RereadingAllegro RereadingAllegro
        //{
        //    get
        //    {
        //        throw new System.NotImplementedException();
        //    }
        //    set
        //    {
        //    }
        //}

        void Closing_MainWindow(object sender, CancelEventArgs e)
        {
            Legend.ClosingLegend = false;
            Legend.Close();
        }

        public Color getColors(int index)
        {
            int value = 255;

            int R = 0;
            int G = 0;
            int B = 0;

            bool newMode = true;
            int counterNewMode = 0;

            bool twoMode = false;
            int counterTwoMode = 0;
            int counterThreeMode = 0;

            for (int i = 0; i <= index; i++)
            {
                if (newMode)
                {
                    switch (counterNewMode)
                    {
                        case 0:
                            R = value;
                            G = 0;
                            B = 0;
                            break;

                        case 1:
                            R = 0;
                            G = value;
                            B = 0;
                            break;

                        case 2:
                            R = 0;
                            G = 0;
                            B = value;
                            break;
                        default: break;
                    }
                    counterNewMode++;
                    if (counterNewMode == 3)
                    {
                        counterNewMode = 0;
                        newMode = false;
                    }

                }
                else
                {
                    if (!twoMode)
                    {
                        switch (counterTwoMode)
                        {
                            case 0:
                                R = value;
                                G = value;
                                B = 0;
                                break;
                            case 1:
                                R = value;
                                G = 0;
                                B = value;
                                break;
                            case 2:
                                R = 0;
                                G = value;
                                B = value;
                                break;
                            default: break;
                        }

                        counterTwoMode++;
                        if (counterTwoMode == 3)
                        {
                            counterTwoMode = 0;
                            twoMode = true;
                        }
                    }
                    else
                    {
                        double ss = 255 / 5;
                        int afterValue = Math.Abs(value - Convert.ToInt32(Math.Floor(ss)));

                        switch (counterThreeMode)
                        {
                            case 0:
                                R = afterValue;
                                G = value;
                                B = value;
                                break;
                            case 1:
                                R = value;
                                G = afterValue;
                                B = value;
                                break;
                            case 2:
                                R = value;
                                G = value;
                                B = afterValue;
                                break;

                            default: break;
                        }


                        counterThreeMode++;
                        if (counterThreeMode == 3)
                        {
                            counterThreeMode = 0;
                            newMode = true;
                            twoMode = false;
                        }

                        value = afterValue;
                        if (value <= 0)
                        {
                            value = 255;
                        }
                    }
                }
            }
            return Color.FromRgb(Convert.ToByte(R), Convert.ToByte(G), Convert.ToByte(B));
        }


        public void CreateRmatrix(int factorSize, int[] ofSet, int sizeSquare, int colorMode)
        {
            //предворительно отчистим канвас
            CanvasRmatrix.Children.Clear();

            CanvasRmatrix.Width = (factorSize + sizeSquare) * Matrix.SizeSquareMatrix + ofSet[0] * 2;
            CanvasRmatrix.Height = (factorSize + sizeSquare) * Matrix.SizeSquareMatrix + ofSet[1] * 2;

            double factorOfSetBaies = 1.4;

            List<TextBlock> listOFElements = new List<TextBlock>();
            //лист с линиями
            List<Path> listOfLine = new List<Path>();

            //для одного из режимов раскраски линий
            int UniqLineColor = 0;

            for (int i = 0; i < Allegro.Elements.Count; i++)
            {


                TextBlock square = new TextBlock();


                //square.Background = new ImageBrush(new BitmapImage(new Uri(@"D:\\rick.jpg")));
                square.Width = sizeSquare;
                square.Height = sizeSquare;
                square.Name = Allegro.Elements[i];
                square.Text = Allegro.Elements[i];
                square.TextAlignment = TextAlignment.Center;
                square.Padding = new Thickness(0, 10, 0, 0);
                square.FontSize = 20;
                square.Background = Brushes.Aqua;


                listOFElements.Add(square);
            }

            for (int i = 0; i < Matrix.SizeSquareMatrix; i++)
            {
                for (int j = 0; j < Matrix.SizeSquareMatrix; j++)
                {
                    if (Matrix.MatrixR[i][j] != -1)
                    {
                        Canvas.SetZIndex(listOFElements.ElementAt(Matrix.MatrixR[i][j]), sizeSquare);
                        Canvas.SetTop(listOFElements.ElementAt(Matrix.MatrixR[i][j]), i * factorSize + ofSet[1]);

                        Canvas.SetZIndex(listOFElements.ElementAt(Matrix.MatrixR[i][j]), sizeSquare);
                        Canvas.SetLeft(listOFElements.ElementAt(Matrix.MatrixR[i][j]), j * factorSize + ofSet[0]);
                        CanvasRmatrix.Children.Add(listOFElements.ElementAt(Matrix.MatrixR[i][j]));
                    }
                }
            }

            // сделаем обход по матрице смежности , а вернее по её половине 

            for (int i = 0; i < Allegro.Elements.Count; i++)
            {
                //j = i указывает на то что мы смотрим только половину
                for (int j = i; j < Allegro.Elements.Count; j++)
                {
                    // используем псевдоусловие что связь есть  
                    //Allegro.Contiguity[i][j] > 0
                    if (Allegro.Contiguity[i][j] > 0 && i != j) // то строим линию
                    {
                        TextBlock A = listOFElements.Find(x => x.Name == Allegro.Elements[i]);
                        TextBlock B = listOFElements.Find(x => x.Name == Allegro.Elements[j]);

                        int A_indexInRmatrix = listOFElements.IndexOf(A);
                        int B_indexInRmatrix = listOFElements.IndexOf(B);

                        int[] A_coordinateInRmatrix = Matrix.getPositionElementFromRmatrix(A_indexInRmatrix);
                        int[] B_coordinateInRmatrix = Matrix.getPositionElementFromRmatrix(B_indexInRmatrix);

                        int absLine = Math.Abs(A_coordinateInRmatrix[0] - B_coordinateInRmatrix[0]);
                        int absColumn = Math.Abs(A_coordinateInRmatrix[1] - B_coordinateInRmatrix[1]);

                        Path newLine = new Path();
                        newLine.StrokeThickness = 1;

                        PathFigure newPathFigure = new PathFigure();
                        int typeConnect = -1;
                        SweepDirection oclock = SweepDirection.Clockwise;
                        Size sizeLine = new Size(); ;

                        // координаты конца и начала линии
                        Point startP = new Point(Canvas.GetLeft(A), Canvas.GetTop(A));
                        Point endP = new Point(Canvas.GetLeft(B), Canvas.GetTop(B));

                        int indexBottomBorder = Matrix.SizeSquareMatrix - 1 - Convert.ToInt32(Math.Floor(Convert.ToDouble((Matrix.SizeSquareMatrix * Matrix.SizeSquareMatrix - Allegro.Elements.Count) / Matrix.SizeSquareMatrix)));

                        Size sizeVertical = new Size(sizeSquare, sizeSquare / 3); ;
                        Size sizeHorizontal = new Size(sizeSquare / 3, sizeSquare);


                        PointCollection myPointCollection = new PointCollection();

                        if (absLine < 2 && absColumn < 2) // элементы соседние 
                        {
                            typeConnect = 0;
                            if (A_coordinateInRmatrix[0] == B_coordinateInRmatrix[0])
                            {
                                startP.Offset(0, sizeSquare / 2);
                                endP.Offset(0, sizeSquare / 2);

                                if (A_coordinateInRmatrix[1] < B_coordinateInRmatrix[1])
                                    startP.Offset(sizeSquare, 0);
                                else endP.Offset(sizeSquare, 0);
                            }

                            if (A_coordinateInRmatrix[1] == B_coordinateInRmatrix[1])
                            {
                                startP.Offset(sizeSquare / 2, 0);
                                endP.Offset(sizeSquare / 2, 0);

                                if (A_coordinateInRmatrix[0] < B_coordinateInRmatrix[0])
                                    startP.Offset(0, sizeSquare);
                                else endP.Offset(0, sizeSquare);
                            }

                            if (A_coordinateInRmatrix[1] != B_coordinateInRmatrix[1] && A_coordinateInRmatrix[0] != B_coordinateInRmatrix[0])
                            {
                                if (A_coordinateInRmatrix[0] < B_coordinateInRmatrix[0])
                                {
                                    if (A_coordinateInRmatrix[1] < B_coordinateInRmatrix[1])
                                        startP.Offset(sizeSquare, sizeSquare);
                                    else
                                    {
                                        startP.Offset(0, sizeSquare);
                                        endP.Offset(sizeSquare, 0);
                                    }
                                }
                                else
                                {
                                    if (A_coordinateInRmatrix[1] < B_coordinateInRmatrix[1])
                                    {
                                        startP.Offset(sizeSquare, 0);
                                        endP.Offset(0, sizeSquare);
                                    }
                                    else endP.Offset(sizeSquare, sizeSquare);
                                }
                            }

                        }// if absLine < 2 && absColumn < 2
                        else //элементы не соседние  
                        {
                            bool A_bottomBorder = false;
                            bool B_bottomBorder = false;

                            //в этом месте мотрим находятся ли элементы у нижнего края
                            if (A_coordinateInRmatrix[0] != Matrix.SizeSquareMatrix - 1)
                            {
                                if (Matrix.MatrixR[A_coordinateInRmatrix[0] + 1][A_coordinateInRmatrix[1]] == -1)
                                    A_bottomBorder = true;
                            }
                            else A_bottomBorder = true;
                            if (B_coordinateInRmatrix[0] != Matrix.SizeSquareMatrix - 1)
                            {
                                if (Matrix.MatrixR[B_coordinateInRmatrix[0] + 1][B_coordinateInRmatrix[1]] == -1)
                                    B_bottomBorder = true;
                            }
                            else B_bottomBorder = true;

                            //узнаём с киких краёв расположены элементы
                            bool LeftTop = (A_coordinateInRmatrix[1] != B_coordinateInRmatrix[1] && A_coordinateInRmatrix[0] != B_coordinateInRmatrix[0])
                                && (A_coordinateInRmatrix[1] == 0 && B_coordinateInRmatrix[0] == 0
                                || B_coordinateInRmatrix[1] == 0 && A_coordinateInRmatrix[0] == 0);

                            bool LeftBottom = (A_coordinateInRmatrix[1] != B_coordinateInRmatrix[1] && A_coordinateInRmatrix[0] != B_coordinateInRmatrix[0])
                                && (A_coordinateInRmatrix[1] == 0 && B_coordinateInRmatrix[0] == indexBottomBorder
                                || B_coordinateInRmatrix[1] == 0 && A_coordinateInRmatrix[0] == indexBottomBorder);

                            bool RightBottom = (A_coordinateInRmatrix[1] != B_coordinateInRmatrix[1] && A_coordinateInRmatrix[0] != B_coordinateInRmatrix[0])
                                && (A_coordinateInRmatrix[1] == Matrix.SizeSquareMatrix - 1 && B_coordinateInRmatrix[0] == indexBottomBorder
                                || B_coordinateInRmatrix[1] == Matrix.SizeSquareMatrix - 1 && A_coordinateInRmatrix[0] == indexBottomBorder);

                            bool RightTop = (A_coordinateInRmatrix[1] != B_coordinateInRmatrix[1] && A_coordinateInRmatrix[0] != B_coordinateInRmatrix[0])
                                && (A_coordinateInRmatrix[0] == 0 && B_coordinateInRmatrix[1] == Matrix.SizeSquareMatrix - 1
                                || B_coordinateInRmatrix[0] == 0 && A_coordinateInRmatrix[1] == Matrix.SizeSquareMatrix - 1);

                            bool LeftRight = !((A_coordinateInRmatrix[0] == B_coordinateInRmatrix[0] && A_coordinateInRmatrix[0] == 0) || (B_bottomBorder && A_bottomBorder))
                                && (A_coordinateInRmatrix[1] == 0 && B_coordinateInRmatrix[1] == Matrix.SizeSquareMatrix - 1
                                || B_coordinateInRmatrix[1] == 0 && A_coordinateInRmatrix[1] == Matrix.SizeSquareMatrix - 1);

                            bool TopBottom = !((A_coordinateInRmatrix[1] == B_coordinateInRmatrix[1] && A_coordinateInRmatrix[1] == 0)
                                || (A_coordinateInRmatrix[1] == B_coordinateInRmatrix[1] && A_coordinateInRmatrix[1] == Matrix.SizeSquareMatrix - 1))
                                && (A_coordinateInRmatrix[0] == 0 && B_coordinateInRmatrix[0] == indexBottomBorder
                                || B_coordinateInRmatrix[0] == 0 && A_coordinateInRmatrix[0] == indexBottomBorder);

                            bool AngleLeftBottomTopRight = A_coordinateInRmatrix[1] == 0 && A_bottomBorder && B_coordinateInRmatrix[1] == Matrix.SizeSquareMatrix - 1 && B_coordinateInRmatrix[0] == 0
                                || B_coordinateInRmatrix[1] == 0 && B_bottomBorder && A_coordinateInRmatrix[1] == Matrix.SizeSquareMatrix - 1 && A_coordinateInRmatrix[0] == 0;

                            bool AngleLeftTopRightBottom = A_coordinateInRmatrix[1] == Matrix.SizeSquareMatrix - 1 && A_bottomBorder && B_coordinateInRmatrix[0] == 0 && B_coordinateInRmatrix[1] == 0
                                || B_coordinateInRmatrix[1] == Matrix.SizeSquareMatrix - 1 && B_bottomBorder && A_coordinateInRmatrix[0] == 0 && A_coordinateInRmatrix[1] == 0;

                            //элементы находятся с разных краёв схемы
                            if (LeftTop || LeftBottom || RightBottom || RightTop || LeftRight || TopBottom)
                            {
                                typeConnect = 2;

                                //элементы находятся в самых крайних позициях квадрата
                                if (AngleLeftBottomTopRight || AngleLeftTopRightBottom)
                                {
                                    if (AngleLeftBottomTopRight)
                                    {
                                        if (A_coordinateInRmatrix[0] < B_coordinateInRmatrix[0])
                                        {
                                            startP.Offset(sizeSquare, sizeSquare);
                                            endP.Offset(sizeSquare, sizeSquare);
                                            myPointCollection.Add(new Point(startP.X + factorSize, endP.Y + factorSize * factorOfSetBaies));
                                            myPointCollection.Add(new Point(startP.X + factorSize, endP.Y + factorSize * factorOfSetBaies));
                                        }
                                        else
                                        {
                                            startP.Offset(sizeSquare, sizeSquare);
                                            endP.Offset(sizeSquare, sizeSquare);
                                            myPointCollection.Add(new Point(endP.X + factorSize, startP.Y + factorSize * factorOfSetBaies));
                                            myPointCollection.Add(new Point(endP.X + factorSize, startP.Y + factorSize * factorOfSetBaies));
                                        }

                                    }
                                    else
                                    {
                                        if (A_coordinateInRmatrix[0] < B_coordinateInRmatrix[0])
                                        {
                                            endP.Offset(sizeSquare, 0);
                                            startP.Offset(sizeSquare, 0);
                                            myPointCollection.Add(new Point(endP.X + factorSize, startP.Y - factorSize * factorOfSetBaies));
                                            myPointCollection.Add(new Point(endP.X + factorSize, startP.Y - factorSize * factorOfSetBaies));
                                        }
                                        else
                                        {
                                            startP.Offset(sizeSquare, 0);
                                            endP.Offset(sizeSquare, 0);
                                            myPointCollection.Add(new Point(startP.X + factorSize, endP.Y - factorSize * factorOfSetBaies));
                                            myPointCollection.Add(new Point(startP.X + factorSize, endP.Y - factorSize * factorOfSetBaies));
                                        }
                                    }
                                }
                                else
                                {
                                    if (LeftTop || LeftBottom || RightBottom || RightTop)
                                    {
                                        if (LeftTop)
                                        {
                                            if (A_coordinateInRmatrix[1] == 0)
                                            {
                                                myPointCollection.Add(new Point(startP.X - factorSize, endP.Y - factorSize * factorOfSetBaies));
                                                myPointCollection.Add(new Point(startP.X - factorSize, endP.Y - factorSize * factorOfSetBaies));
                                            }
                                            else
                                            {
                                                if (B_bottomBorder)
                                                {
                                                    typeConnect = 0;
                                                }
                                                else
                                                {

                                                    myPointCollection.Add(new Point(endP.X - factorSize * factorOfSetBaies, startP.Y - factorSize));
                                                    myPointCollection.Add(new Point(endP.X - factorSize * factorOfSetBaies, startP.Y - factorSize));
                                                }
                                            }

                                        }

                                        if (LeftBottom)
                                        {
                                            if (A_coordinateInRmatrix[1] == 0)
                                            {
                                                startP.Offset(0, sizeSquare);
                                                endP.Offset(0, sizeSquare);
                                                myPointCollection.Add(new Point(startP.X - factorSize, endP.Y + factorSize * factorOfSetBaies));
                                                myPointCollection.Add(new Point(startP.X - factorSize, endP.Y + factorSize * factorOfSetBaies));
                                            }
                                            else
                                            {
                                                startP.Offset(0, sizeSquare);
                                                endP.Offset(0, sizeSquare);
                                                myPointCollection.Add(new Point(endP.X - factorSize * factorOfSetBaies, startP.Y + factorSize));
                                                myPointCollection.Add(new Point(endP.X - factorSize * factorOfSetBaies, startP.Y + factorSize));
                                            }
                                        }
                                        if (RightBottom)
                                        {
                                            if (A_coordinateInRmatrix[1] == Matrix.SizeSquareMatrix - 1)
                                            {
                                                startP.Offset(sizeSquare, sizeSquare);
                                                endP.Offset(sizeSquare, sizeSquare);
                                                myPointCollection.Add(new Point(startP.X + factorSize, endP.Y + factorSize * factorOfSetBaies));
                                                myPointCollection.Add(new Point(startP.X + factorSize, endP.Y + factorSize * factorOfSetBaies));
                                            }
                                            else
                                            {
                                                startP.Offset(sizeSquare, sizeSquare);
                                                endP.Offset(sizeSquare, sizeSquare);
                                                myPointCollection.Add(new Point(endP.X + factorSize * factorOfSetBaies, startP.Y + factorSize));
                                                myPointCollection.Add(new Point(endP.X + factorSize * factorOfSetBaies, startP.Y + factorSize));
                                            }
                                        }
                                        if (RightTop)
                                        {
                                            if (A_coordinateInRmatrix[1] == Matrix.SizeSquareMatrix - 1)
                                            {
                                                startP.Offset(sizeSquare, 0);
                                                endP.Offset(sizeSquare, 0);
                                                myPointCollection.Add(new Point(startP.X + factorSize, endP.Y - factorSize * factorOfSetBaies));
                                                myPointCollection.Add(new Point(startP.X + factorSize, endP.Y - factorSize * factorOfSetBaies));
                                            }
                                            else
                                            {
                                                startP.Offset(sizeSquare, 0);
                                                endP.Offset(sizeSquare, 0);
                                                myPointCollection.Add(new Point(endP.X + factorSize * factorOfSetBaies, startP.Y - factorSize));
                                                myPointCollection.Add(new Point(endP.X + factorSize * factorOfSetBaies, startP.Y - factorSize));
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (TopBottom)
                                        {
                                            if (A_coordinateInRmatrix[1] < B_coordinateInRmatrix[1]) // стартовый элемент находится левее конечного
                                            {
                                                if (A_coordinateInRmatrix[0] > B_coordinateInRmatrix[0]) // стартовый элемент ниже конечного
                                                {
                                                    if (A_coordinateInRmatrix[1] > Matrix.SizeSquareMatrix / 2 - 1) // стартовый элемент в правой стороне от центра
                                                    {
                                                        startP.Offset(sizeSquare, sizeSquare);
                                                        endP.Offset(sizeSquare, 0);
                                                        int edgePointX = (Matrix.SizeSquareMatrix - A_coordinateInRmatrix[1]) * (Convert.ToInt32(factorSize)) + ofSet[0];
                                                        myPointCollection.Add(new Point(startP.X + edgePointX, startP.Y + factorSize * (Matrix.SizeSquareMatrix - A_coordinateInRmatrix[1] - 1)));
                                                        myPointCollection.Add(new Point(startP.X + edgePointX, endP.Y - factorSize * (Matrix.SizeSquareMatrix - A_coordinateInRmatrix[1] - 1)));
                                                    }
                                                    else // стартовый элемент в левой стороне от центра
                                                    {
                                                        startP.Offset(sizeSquare, sizeSquare);
                                                        endP.Offset(sizeSquare, 0);
                                                        int edgePointX = (Matrix.SizeSquareMatrix - A_coordinateInRmatrix[1]) * (Convert.ToInt32(factorSize)) + ofSet[0];
                                                        myPointCollection.Add(new Point(startP.X + edgePointX, startP.Y + factorSize * (Matrix.SizeSquareMatrix - A_coordinateInRmatrix[1])));
                                                        myPointCollection.Add(new Point(startP.X + edgePointX, endP.Y - factorSize * (Matrix.SizeSquareMatrix - A_coordinateInRmatrix[1] - 1)));
                                                    }
                                                }
                                                else // стартовый элемент выше конечного
                                                {
                                                    if (A_coordinateInRmatrix[1] > Matrix.SizeSquareMatrix / 2 - 1)
                                                    {
                                                        endP.Offset(0, sizeSquare);
                                                        int edgePointX = (Matrix.SizeSquareMatrix - A_coordinateInRmatrix[1]) * (Convert.ToInt32(factorSize)) + ofSet[0];
                                                        myPointCollection.Add(new Point(startP.X - edgePointX, startP.Y - factorSize * (Matrix.SizeSquareMatrix - A_coordinateInRmatrix[1])));
                                                        myPointCollection.Add(new Point(startP.X - edgePointX, endP.Y + factorSize * (Matrix.SizeSquareMatrix - A_coordinateInRmatrix[1] + 1)));
                                                    }
                                                    else
                                                    {
                                                        endP.Offset(0, sizeSquare);
                                                        int edgePointX = A_coordinateInRmatrix[1] * (Convert.ToInt32(factorSize)) + ofSet[0];
                                                        myPointCollection.Add(new Point(startP.X - edgePointX, startP.Y - factorSize * (Matrix.SizeSquareMatrix - A_coordinateInRmatrix[1] - 1)));
                                                        myPointCollection.Add(new Point(startP.X - edgePointX, endP.Y + factorSize * (Matrix.SizeSquareMatrix - A_coordinateInRmatrix[1])));
                                                    }
                                                }

                                            }
                                            else  // стартовый элемент находится правее конечного
                                            {
                                                if (A_coordinateInRmatrix[0] > B_coordinateInRmatrix[0]) // стартовый элемент ниже конечного
                                                {
                                                    if (A_coordinateInRmatrix[1] > Matrix.SizeSquareMatrix / 2 - 1) // стартовый элемент в правой стороне от центра
                                                    {
                                                        startP.Offset(sizeSquare, sizeSquare);
                                                        endP.Offset(sizeSquare, 0);
                                                        int edgePointX = (Matrix.SizeSquareMatrix - A_coordinateInRmatrix[1]) * (Convert.ToInt32(factorSize)) + ofSet[0];
                                                        myPointCollection.Add(new Point(startP.X + edgePointX, startP.Y + factorSize * (Matrix.SizeSquareMatrix - A_coordinateInRmatrix[1] - 1)));
                                                        myPointCollection.Add(new Point(startP.X + edgePointX, endP.Y - factorSize * (Matrix.SizeSquareMatrix - A_coordinateInRmatrix[1])));
                                                    }
                                                    else // стартовый элемент в левой стороне от центра
                                                    {
                                                        startP.Offset(sizeSquare, sizeSquare);
                                                        endP.Offset(sizeSquare, 0);
                                                        int edgePointX = (A_coordinateInRmatrix[1]) * (Convert.ToInt32(factorSize)) + ofSet[0];
                                                        myPointCollection.Add(new Point(startP.X - edgePointX, startP.Y + factorSize * (Matrix.SizeSquareMatrix - A_coordinateInRmatrix[1])));
                                                        myPointCollection.Add(new Point(startP.X - edgePointX, endP.Y - factorSize * (Matrix.SizeSquareMatrix - A_coordinateInRmatrix[1])));
                                                    }
                                                }
                                                else // стартовый элемент выше конечного
                                                {
                                                    if (A_coordinateInRmatrix[1] > Matrix.SizeSquareMatrix / 2 - 1)
                                                    {
                                                        endP.Offset(sizeSquare, sizeSquare);
                                                        int edgePointX = (Matrix.SizeSquareMatrix - A_coordinateInRmatrix[1]) * (Convert.ToInt32(factorSize)) + ofSet[0];
                                                        myPointCollection.Add(new Point(startP.X + edgePointX, startP.Y - factorSize * (Matrix.SizeSquareMatrix - A_coordinateInRmatrix[1] + 1)));
                                                        myPointCollection.Add(new Point(startP.X + edgePointX, endP.Y + factorSize * (Matrix.SizeSquareMatrix - A_coordinateInRmatrix[1] + 1)));
                                                    }
                                                    else
                                                    {
                                                        endP.Offset(0, sizeSquare);
                                                        int edgePointX = A_coordinateInRmatrix[1] * (Convert.ToInt32(factorSize)) + ofSet[0];
                                                        myPointCollection.Add(new Point(startP.X - edgePointX, startP.Y - factorSize * (Matrix.SizeSquareMatrix - A_coordinateInRmatrix[1])));
                                                        myPointCollection.Add(new Point(startP.X - edgePointX, endP.Y + factorSize * (Matrix.SizeSquareMatrix - A_coordinateInRmatrix[1])));
                                                    }
                                                }
                                            }
                                        }
                                        if (LeftRight)
                                        {
                                            if (A_coordinateInRmatrix[1] < B_coordinateInRmatrix[1]) // стартовый элемент находится левее конечного
                                            {
                                                if (A_coordinateInRmatrix[0] > B_coordinateInRmatrix[0]) // стартовый элемент ниже конечного
                                                {
                                                    if (A_coordinateInRmatrix[0] > Matrix.SizeSquareMatrix / 2 - 1)
                                                    {
                                                        startP.Offset(0, sizeSquare);
                                                        endP.Offset(sizeSquare, sizeSquare);
                                                        int edgePointY = (Matrix.SizeSquareMatrix - A_coordinateInRmatrix[0]) * (Convert.ToInt32(factorSize)) + ofSet[1];
                                                        myPointCollection.Add(new Point(startP.X - factorSize * (Matrix.SizeSquareMatrix - A_coordinateInRmatrix[0] + 1), startP.Y + edgePointY));
                                                        myPointCollection.Add(new Point(endP.X + factorSize * (Matrix.SizeSquareMatrix - A_coordinateInRmatrix[0] + 1), startP.Y + edgePointY));
                                                    }
                                                    else
                                                    {
                                                        endP.Offset(sizeSquare, 0);
                                                        int edgePointY = (A_coordinateInRmatrix[0]) * (Convert.ToInt32(factorSize)) + ofSet[1];
                                                        myPointCollection.Add(new Point(startP.X - factorSize * (Matrix.SizeSquareMatrix - B_coordinateInRmatrix[0] + 1), startP.Y - edgePointY));
                                                        myPointCollection.Add(new Point(endP.X + factorSize * (Matrix.SizeSquareMatrix - A_coordinateInRmatrix[0] + 1), startP.Y - edgePointY));
                                                    }

                                                }
                                                else // стартовый элемент выше конечного
                                                {
                                                    endP.Offset(sizeSquare, 0);
                                                    int edgePointY = (A_coordinateInRmatrix[0] + 1) * (Convert.ToInt32(factorSize)) + ofSet[1];
                                                    myPointCollection.Add(new Point(startP.X - factorSize * (Matrix.SizeSquareMatrix - A_coordinateInRmatrix[0] + 1), startP.Y - edgePointY));
                                                    myPointCollection.Add(new Point(endP.X + factorSize * (Matrix.SizeSquareMatrix - A_coordinateInRmatrix[0] + 1), startP.Y - edgePointY));
                                                }
                                            }
                                            else // стартовый элемент находится правее конечного 
                                            {
                                                if (A_coordinateInRmatrix[0] > B_coordinateInRmatrix[0]) // стартовый элемент ниже конечного
                                                {
                                                    if (A_coordinateInRmatrix[0] > Matrix.SizeSquareMatrix / 2 - 1)
                                                    {
                                                        endP.Offset(0, sizeSquare);
                                                        startP.Offset(sizeSquare, sizeSquare);
                                                        int edgePointY = (Matrix.SizeSquareMatrix - A_coordinateInRmatrix[0]) * (Convert.ToInt32(factorSize)) + ofSet[1];
                                                        myPointCollection.Add(new Point(startP.X + factorSize * (Matrix.SizeSquareMatrix - A_coordinateInRmatrix[0] + 1), startP.Y + edgePointY));
                                                        myPointCollection.Add(new Point(endP.X - factorSize * (Matrix.SizeSquareMatrix - A_coordinateInRmatrix[0] + 1), startP.Y + edgePointY));
                                                    }
                                                    else
                                                    {
                                                        startP.Offset(sizeSquare, 0);
                                                        int edgePointY = (A_coordinateInRmatrix[0]) * (Convert.ToInt32(factorSize)) + ofSet[1];
                                                        myPointCollection.Add(new Point(startP.X + factorSize * (Matrix.SizeSquareMatrix - B_coordinateInRmatrix[0] + 1), startP.Y - edgePointY));
                                                        myPointCollection.Add(new Point(endP.X - factorSize * (Matrix.SizeSquareMatrix - A_coordinateInRmatrix[0] + 1), startP.Y - edgePointY));
                                                    }
                                                }
                                                else // стартовый элемент выше конечного
                                                {
                                                    if (A_coordinateInRmatrix[0] > Matrix.SizeSquareMatrix / 2 - 1)
                                                    {
                                                        endP.Offset(0, sizeSquare);
                                                        startP.Offset(sizeSquare, sizeSquare);
                                                        int edgePointY = (A_coordinateInRmatrix[0]) * (Convert.ToInt32(factorSize)) + ofSet[1];
                                                        myPointCollection.Add(new Point(startP.X + factorSize * (Matrix.SizeSquareMatrix - A_coordinateInRmatrix[0] + 1), startP.Y + edgePointY));
                                                        myPointCollection.Add(new Point(endP.X - factorSize * (Matrix.SizeSquareMatrix - A_coordinateInRmatrix[0] + 1), startP.Y + edgePointY));
                                                    }
                                                    else
                                                    {
                                                        startP.Offset(sizeSquare, 0);
                                                        int edgePointY = (A_coordinateInRmatrix[0] + 1) * (Convert.ToInt32(factorSize)) + ofSet[1];
                                                        myPointCollection.Add(new Point(startP.X + factorSize * (Matrix.SizeSquareMatrix - B_coordinateInRmatrix[0] + 1), startP.Y - edgePointY));
                                                        myPointCollection.Add(new Point(endP.X - factorSize * (Matrix.SizeSquareMatrix - A_coordinateInRmatrix[0] + 1), startP.Y - edgePointY));
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else // элементы находятся не с разных краёв схемы
                            {
                                typeConnect = 1;
                                // элементы находящиеся в одном ряду по горизонтали или вертикали
                                if (A_coordinateInRmatrix[0] == B_coordinateInRmatrix[0] || A_coordinateInRmatrix[1] == B_coordinateInRmatrix[1])
                                {
                                    //определяем находятся ли элементы у края
                                    bool ColumnLeft = A_coordinateInRmatrix[1] == 0 && A_coordinateInRmatrix[1] == B_coordinateInRmatrix[1];
                                    bool ColumnRight = A_coordinateInRmatrix[1] == Matrix.SizeSquareMatrix - 1 && A_coordinateInRmatrix[1] == B_coordinateInRmatrix[1];
                                    bool LineTop = A_coordinateInRmatrix[0] == 0 && A_coordinateInRmatrix[0] == B_coordinateInRmatrix[0];
                                    bool LineBottom = A_coordinateInRmatrix[0] == B_coordinateInRmatrix[0] && A_coordinateInRmatrix[0] == indexBottomBorder;



                                    if (ColumnLeft || ColumnRight || LineTop || LineBottom)
                                    {
                                        //левая колонка
                                        if (ColumnLeft)
                                        {
                                            sizeLine = sizeVertical;
                                            if (A_coordinateInRmatrix[0] < B_coordinateInRmatrix[0])
                                            {
                                                startP.Offset(0, sizeSquare);
                                                oclock = SweepDirection.Counterclockwise;
                                            }
                                            else
                                            {
                                                endP.Offset(0, sizeSquare);
                                                oclock = SweepDirection.Clockwise;
                                            }
                                        }

                                        //правая колонка
                                        if (ColumnRight)
                                        {
                                            sizeLine = sizeVertical;
                                            if (A_coordinateInRmatrix[0] < B_coordinateInRmatrix[0])
                                            {
                                                startP.Offset(sizeSquare, sizeSquare);
                                                endP.Offset(sizeSquare, 0);
                                                oclock = SweepDirection.Clockwise;
                                            }
                                            else
                                            {
                                                startP.Offset(sizeSquare, 0);
                                                endP.Offset(sizeSquare, sizeSquare);
                                                oclock = SweepDirection.Counterclockwise;
                                            }
                                        }

                                        //верхняя полоса
                                        if (LineTop)
                                        {
                                            sizeLine = sizeHorizontal;
                                            if (A_coordinateInRmatrix[1] < B_coordinateInRmatrix[1])
                                            {
                                                startP.Offset(sizeSquare, 0);
                                                oclock = SweepDirection.Clockwise;
                                            }
                                            else
                                            {
                                                endP.Offset(sizeSquare, 0);
                                                oclock = SweepDirection.Counterclockwise;
                                            }
                                        }

                                        //нижняя полоса
                                        if (LineBottom)
                                        {
                                            sizeLine = sizeHorizontal;
                                            if (A_coordinateInRmatrix[1] < B_coordinateInRmatrix[1])
                                            {
                                                startP.Offset(sizeSquare, sizeSquare);
                                                endP.Offset(0, sizeSquare);
                                                oclock = SweepDirection.Counterclockwise;
                                            }
                                            else
                                            {
                                                endP.Offset(sizeSquare, sizeSquare);
                                                startP.Offset(0, sizeSquare);
                                                oclock = SweepDirection.Clockwise;
                                            }
                                        }
                                    }
                                    else // элементы находятся не у края  
                                    {
                                        //элементы в одной линии
                                        if (A_coordinateInRmatrix[0] == B_coordinateInRmatrix[0])
                                        {
                                            sizeLine = sizeHorizontal;
                                            if (A_coordinateInRmatrix[0] > Matrix.SizeSquareMatrix / 2 - 1)
                                            {
                                                if (A_coordinateInRmatrix[1] > B_coordinateInRmatrix[1])
                                                {
                                                    startP.Offset(0, sizeSquare);
                                                    endP.Offset(sizeSquare, sizeSquare);
                                                    oclock = SweepDirection.Clockwise;
                                                }
                                                else
                                                {
                                                    startP.Offset(sizeSquare, sizeSquare);
                                                    endP.Offset(0, sizeSquare);
                                                    oclock = SweepDirection.Counterclockwise;
                                                }
                                            }
                                            else
                                            {
                                                if (A_coordinateInRmatrix[1] > B_coordinateInRmatrix[1])
                                                {
                                                    endP.Offset(sizeSquare, 0);
                                                    oclock = SweepDirection.Counterclockwise;
                                                }
                                                else
                                                {
                                                    startP.Offset(sizeSquare, 0);
                                                    oclock = SweepDirection.Clockwise;
                                                }
                                            }
                                        }
                                        else // элементы в одной колонке
                                        {
                                            sizeLine = sizeVertical;
                                            if (A_coordinateInRmatrix[1] > Matrix.SizeSquareMatrix / 2 - 1)
                                            {
                                                if (A_coordinateInRmatrix[0] > B_coordinateInRmatrix[0])
                                                {
                                                    startP.Offset(sizeSquare, 0);
                                                    endP.Offset(sizeSquare, sizeSquare);
                                                    oclock = SweepDirection.Counterclockwise;
                                                }
                                                else
                                                {
                                                    startP.Offset(sizeSquare, sizeSquare);
                                                    endP.Offset(sizeSquare, 0);
                                                    oclock = SweepDirection.Clockwise;
                                                }
                                            }
                                            else
                                            {
                                                if (A_coordinateInRmatrix[0] > B_coordinateInRmatrix[0])
                                                {
                                                    endP.Offset(0, sizeSquare);
                                                    oclock = SweepDirection.Clockwise;
                                                }
                                                else
                                                {
                                                    startP.Offset(0, sizeSquare);
                                                    oclock = SweepDirection.Counterclockwise;
                                                }
                                            }
                                        }
                                    }
                                }
                                else // элементы находящиеся в соседних строках или колонках
                                {
                                    if (absLine < 2 || absColumn < 2)
                                    {
                                        typeConnect = 0;
                                        if (absLine < 2)
                                        {
                                            if (A_coordinateInRmatrix[1] < B_coordinateInRmatrix[1])
                                            {
                                                if (A_coordinateInRmatrix[0] < B_coordinateInRmatrix[0])
                                                {
                                                    startP.Offset(sizeSquare, sizeSquare);
                                                }
                                                else
                                                {
                                                    startP.Offset(sizeSquare, 0);
                                                    endP.Offset(0, sizeSquare);
                                                }
                                            }
                                            else
                                            {
                                                if (A_coordinateInRmatrix[0] < B_coordinateInRmatrix[0])
                                                {
                                                    startP.Offset(0, sizeSquare);
                                                    endP.Offset(sizeSquare, 0);
                                                }
                                                else
                                                {
                                                    endP.Offset(sizeSquare, sizeSquare);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (A_coordinateInRmatrix[0] < B_coordinateInRmatrix[0])
                                            {
                                                if (A_coordinateInRmatrix[1] < B_coordinateInRmatrix[1])
                                                {
                                                    startP.Offset(sizeSquare, sizeSquare);
                                                }
                                                else
                                                {
                                                    startP.Offset(0, sizeSquare);
                                                    endP.Offset(sizeSquare, 0);
                                                }
                                            }
                                            else
                                            {
                                                if (A_coordinateInRmatrix[1] < B_coordinateInRmatrix[1])
                                                {
                                                    startP.Offset(sizeSquare, 0);
                                                    endP.Offset(0, sizeSquare);
                                                }
                                                else
                                                {
                                                    endP.Offset(sizeSquare, sizeSquare);
                                                }
                                            }
                                        }
                                    }
                                    else // здесь будут все остальные виды связей элементов
                                    {
                                        typeConnect = 2;
                                        //элементы
                                        if (A_coordinateInRmatrix[0] < B_coordinateInRmatrix[0])
                                        {
                                            if (A_coordinateInRmatrix[1] < B_coordinateInRmatrix[1])
                                            {
                                                startP.Offset(sizeSquare, sizeSquare);
                                                myPointCollection.Add(new Point(startP.X + factorSize, startP.Y));
                                                //myPointCollection.Add(new Point(endP.X - (factorSize / 2), startP.Y + (factorSize / 2)));
                                                myPointCollection.Add(new Point(endP.X, endP.Y - factorSize));
                                            }
                                            else
                                            {
                                                startP.Offset(0, sizeSquare);
                                                endP.Offset(sizeSquare, 0);
                                                myPointCollection.Add(new Point(startP.X - factorSize, startP.Y));
                                                myPointCollection.Add(new Point(endP.X, endP.Y - factorSize));
                                            }
                                        }
                                        else
                                        {
                                            if (A_coordinateInRmatrix[1] < B_coordinateInRmatrix[1])
                                            {
                                                startP.Offset(sizeSquare, 0);
                                                endP.Offset(0, sizeSquare);
                                                myPointCollection.Add(new Point(startP.X, startP.Y - factorSize));
                                                myPointCollection.Add(new Point(endP.X - factorSize, endP.Y));
                                            }
                                            else
                                            {
                                                endP.Offset(sizeSquare, sizeSquare);
                                                myPointCollection.Add(new Point(startP.X, startP.Y - factorSize));
                                                myPointCollection.Add(new Point(endP.X + factorSize, endP.Y));
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        // добовление линии 
                        newPathFigure.StartPoint = startP;

                        switch (typeConnect)
                        {
                            case 0:
                                newPathFigure.Segments.Add(
                                    new LineSegment(
                                        endP,
                                        true /* IsStroked */ ));

                                break;
                            case 1:

                                newPathFigure.Segments.Add(
                                new ArcSegment(
                                    endP,
                                    sizeLine,
                                    89,
                                    false, /* IsLargeArc */
                                    oclock,
                                    true /* IsStroked */ ));

                                break;
                            case 2:

                                myPointCollection.Add(endP);
                                newPathFigure.Segments.Add(
                                new PolyBezierSegment(myPointCollection, true));

                                break;
                            default:
                                break;

                        }

                        PathGeometry newPathGeometry = new PathGeometry();
                        newPathGeometry.Figures.Add(newPathFigure);
                        newLine.Data = newPathGeometry;


                        // задание цвета линии
                        switch (colorMode)
                        {
                            //монотон
                            case 0:
                                newLine.Stroke = Brushes.Tomato;
                                break;
                            //в зависимости от веса ребра
                            case 1:
                                //SolidColorBrush colorWeight = new SolidColorBrush()
                                newLine.Stroke = new SolidColorBrush(getColors(Allegro.Contiguity[i][j]));
                                break;
                            //в завасимоти от типа соседства
                            case 2:
                                newLine.Stroke = new SolidColorBrush(getColors(typeConnect));
                                break;
                            //в завасимоти от дальности между элементами
                            case 3:
                                newLine.Stroke = new SolidColorBrush(getColors(Matrix.getDuration(B_indexInRmatrix, A_indexInRmatrix) - 1));
                                break;
                            //уникальный цвет
                            case 4:
                                newLine.Stroke = new SolidColorBrush(getColors(UniqLineColor));
                                UniqLineColor++;
                                break;

                            default:
                                break;
                        }

                        listOfLine.Add(newLine);
                    }
                }
            }

            for (int i = 0; i < listOfLine.Count; i++)
            {
                Canvas.SetZIndex(listOfLine[i], 100);
                CanvasRmatrix.Children.Add(listOfLine[i]);
            }
        }

        public void ClearAndHidden()
        {
            GridAlgoritm.Visibility = Visibility.Hidden;
            ButtonRaspredelenia.Visibility = Visibility.Hidden;
            LabelOfBeforeQuality.Visibility = Visibility.Hidden;
            TextBoxOfBeforeQuality.Visibility = Visibility.Hidden;
            TextBoxOfBeforeQuality.Text = "";
            LabelOfAfterQuality.Visibility = Visibility.Hidden;
            TextBoxOfAfterQuality.Visibility = Visibility.Hidden;
            TextBoxOfAfterQuality.Text = "";
            ButtonPerestanovki.Visibility = Visibility.Hidden;
            TextBoxOfCountIterac.Visibility = Visibility.Hidden;
            TextBoxOfCountIterac.Text = "5";
            LabelOfCountPerestanovki.Visibility = Visibility.Hidden;

            MenuColorMode.Visibility = Visibility.Hidden;
            PaddingEdge.Visibility = Visibility.Hidden;
            PaddingEdgeX.Visibility = Visibility.Hidden;
            PaddingEdgeXValue.Visibility = Visibility.Hidden;

            PaddingEdgeY.Visibility = Visibility.Hidden;
            PaddingEdgeYValue.Visibility = Visibility.Hidden;

            PaddingEdgeBottonLeft.Visibility = Visibility.Hidden;
            PaddingEdgeBottonRight.Visibility = Visibility.Hidden;
            PaddingEdgeBottonTop.Visibility = Visibility.Hidden;
            PaddingEdgeBottonBottom.Visibility = Visibility.Hidden;
            FactorSizeText.Visibility = Visibility.Hidden;
            FactorSizeValue.Visibility = Visibility.Hidden;

            SizeSquareText.Visibility = Visibility.Hidden;
            SizeSquareValue.Visibility = Visibility.Hidden;


            FactorSizeValueButtonMinus.Visibility = Visibility.Hidden;
            FactorSizeValueButtonPlus.Visibility = Visibility.Hidden;
            SizeSquareValueButtonMinus.Visibility = Visibility.Hidden;
            SizeSquareValueButtonPlus.Visibility = Visibility.Hidden;
            LegendLine.Visibility = Visibility.Hidden;

            PaddingEdgeXValue.Text = "100";
            PaddingEdgeYValue.Text = "100";
            FactorSizeValue.Text = "100";
            SizeSquareValue.Text = "50";

            Contiguity.Text = "";
            CanvasRmatrix.Children.Clear();
        }
        public void StepFirst() // шаг на котором пользователь уже выбрал файл 
        {
            GridAlgoritm.Visibility = Visibility.Visible;
            ButtonRaspredelenia.Visibility = Visibility.Visible;
        }

        public void StepSecond() // нажата кнопка распределить 
        {
            ButtonPerestanovki.Visibility = Visibility.Visible;
            LabelOfBeforeQuality.Visibility = Visibility.Visible;
            TextBoxOfBeforeQuality.Visibility = Visibility.Visible;
            TextBoxOfCountIterac.Visibility = Visibility.Visible;
            LabelOfCountPerestanovki.Visibility = Visibility.Visible;

            MenuColorMode.Visibility = Visibility.Visible;
            PaddingEdge.Visibility = Visibility.Visible;
            PaddingEdgeX.Visibility = Visibility.Visible;
            PaddingEdgeXValue.Visibility = Visibility.Visible;

            PaddingEdgeY.Visibility = Visibility.Visible;
            PaddingEdgeYValue.Visibility = Visibility.Visible;

            PaddingEdgeBottonLeft.Visibility = Visibility.Visible;
            PaddingEdgeBottonRight.Visibility = Visibility.Visible;
            PaddingEdgeBottonTop.Visibility = Visibility.Visible;
            PaddingEdgeBottonBottom.Visibility = Visibility.Visible;
            FactorSizeText.Visibility = Visibility.Visible;
            FactorSizeValue.Visibility = Visibility.Visible;

            FactorSizeValueButtonMinus.Visibility = Visibility.Visible;
            FactorSizeValueButtonPlus.Visibility = Visibility.Visible;
            SizeSquareValueButtonMinus.Visibility = Visibility.Visible;
            SizeSquareValueButtonPlus.Visibility = Visibility.Visible;

            SizeSquareText.Visibility = Visibility.Visible;
            SizeSquareValue.Visibility = Visibility.Visible;
            LegendLine.Visibility = Visibility.Visible;


        }
        public void StepSThird() // нажата кнопка переставить
        {
            LabelOfAfterQuality.Visibility = Visibility.Visible;
            TextBoxOfAfterQuality.Visibility = Visibility.Visible;
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e) // открыть файл
        {
            OpenFileDialog myDialog = new OpenFileDialog();
            myDialog.Filter = "Text files (*.net)|*.net|All files (*.*)|*.*";

            if (myDialog.ShowDialog() == true)
            {
                ClearAndHidden();
                Allegro = new FileReader(myDialog.FileName);
                int[][] ContiguityMass = Allegro.Contiguity;
                string[] ElementsMss = Allegro.Elements.ToArray();
                string ContiguityText = "    ";
                Contiguity.Text = "";
                for (int i = -1; i < ContiguityMass.Length; i++)
                {
                    if (i != -1)
                    {
                        ContiguityText += ElementsMss[i].Length < 3 ? ElementsMss[i] + "  " : ElementsMss[i] + " ";
                    }
                    for (int j = 0; j < ContiguityMass.Length; j++)
                    {
                        if (i == -1)
                        {
                            ContiguityText += ElementsMss[j].Length < 3 ? ElementsMss[j] + "  " : ElementsMss[j] + " ";
                        }
                        else
                            ContiguityText += " " + Convert.ToString(Allegro.Contiguity[i][j]) + "  ";
                    }
                    ContiguityText += "\n";
                }
                Contiguity.Text = ContiguityText;
                StepFirst();
            }
        }

        public void doit()
        {
            int[][] ContiguityMass = Allegro.Contiguity;
            string[] ElementsMss = Allegro.Elements.ToArray();
            string ContiguityText = "    ";
            Contiguity.Text = "";
            for (int i = -1; i < ContiguityMass.Length; i++)
            {
                if (i != -1)
                {
                    ContiguityText += ElementsMss[i].Length < 3 ? ElementsMss[i] + "  " : ElementsMss[i] + " ";
                }
                for (int j = 0; j < ContiguityMass.Length; j++)
                {
                    if (i == -1)
                    {
                        ContiguityText += ElementsMss[j].Length < 3 ? ElementsMss[j] + "  " : ElementsMss[j] + " ";
                    }
                    else
                        ContiguityText += " " + Convert.ToString(Allegro.Contiguity[i][j]) + "  ";
                }
                ContiguityText += "\n";
            }
            Contiguity.Text = ContiguityText;
            StepFirst();
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e) // очистить 
        {
            ClearAndHidden();
        }

        private void MenuItem_Click_2(object sender, RoutedEventArgs e) // выход из приложения
        {
            this.Close();
        }

        private void ButtonRaspredelenia_Click(object sender, RoutedEventArgs e) // кнопка рапределения
        {
            Matrix = new CoherentIterative(Allegro.Contiguity);
            Matrix.createMatrixR();
            TextBoxOfBeforeQuality.Text = Convert.ToString(Matrix.QualityBefore);
            //CreateRmatrix(factorSize, ofSet, sizeSquare, colorMode);

            CreateRmatrix(Convert.ToInt32(FactorSizeValue.Text),
                new int[] { Convert.ToInt32(PaddingEdgeXValue.Text), 
                    Convert.ToInt32(PaddingEdgeYValue.Text) }, Convert.ToInt32(SizeSquareValue.Text), colorMode);
            //Legend.SetContent(new Color[]{Brushes.Tomato.Color}, new string[] { "связь присутствует" });
            StepSecond();
            // обновить легенду
            ItemCollection col = MenuColorMode.Items;
            for (int i = 0; i < col.Count; i++)
            {
                MenuItem s = (MenuItem)col.GetItemAt(i);
                string w = (string)s.Header;
                string w1 = (string)MenuColorMode.Header;
                w1 = w1.Split(':')[1].Trim();
                if (w1.ToLower() == w.ToLower())
                {
                    Universe_change_Colors(s, null);
                }
            }
        }

        private void ButtonPerestanovki_Click(object sender, RoutedEventArgs e)
        {
            Matrix.optimizationMatrixR(Convert.ToInt32(TextBoxOfCountIterac.Text));
            TextBoxOfAfterQuality.Text = Convert.ToString(Matrix.QualityAfter);
            CreateRmatrix(Convert.ToInt32(FactorSizeValue.Text),
                new int[] { Convert.ToInt32(PaddingEdgeXValue.Text), 
                    Convert.ToInt32(PaddingEdgeYValue.Text) }, Convert.ToInt32(SizeSquareValue.Text), colorMode);
            StepSThird();
        }
        private void Univers_KeyDown(object sender, KeyEventArgs e)
        {


            if (e.Key == Key.Return)
            {
                CreateRmatrix(Convert.ToInt32(FactorSizeValue.Text),
                new int[] { Convert.ToInt32(PaddingEdgeXValue.Text), 
                    Convert.ToInt32(PaddingEdgeYValue.Text) }, Convert.ToInt32(SizeSquareValue.Text), colorMode);
            }
            else
            {
                if (!System.Text.RegularExpressions.Regex.Match(e.Key.ToString(), @"[0-9]").Success)
                {
                    e.Handled = true;
                }
            }
        }

        private void PaddingEdgeBottonTop_Click(object sender, RoutedEventArgs e)
        {
            PaddingEdgeYValue.Text = Convert.ToString(Convert.ToInt32(PaddingEdgeYValue.Text) - 10);
            CreateRmatrix(Convert.ToInt32(FactorSizeValue.Text),
                new int[] { Convert.ToInt32(PaddingEdgeXValue.Text), 
                    Convert.ToInt32(PaddingEdgeYValue.Text) }, Convert.ToInt32(SizeSquareValue.Text), colorMode);
        }

        private void PaddingEdgeBottonBottom_Click(object sender, RoutedEventArgs e)
        {
            PaddingEdgeYValue.Text = Convert.ToString(Convert.ToInt32(PaddingEdgeYValue.Text) + 10);
            CreateRmatrix(Convert.ToInt32(FactorSizeValue.Text),
                new int[] { Convert.ToInt32(PaddingEdgeXValue.Text), 
                    Convert.ToInt32(PaddingEdgeYValue.Text) }, Convert.ToInt32(SizeSquareValue.Text), colorMode);
        }

        private void PaddingEdgeBottonLeft_Click(object sender, RoutedEventArgs e)
        {
            PaddingEdgeXValue.Text = Convert.ToString(Convert.ToInt32(PaddingEdgeXValue.Text) - 10);
            CreateRmatrix(Convert.ToInt32(FactorSizeValue.Text),
                new int[] { Convert.ToInt32(PaddingEdgeXValue.Text), 
                    Convert.ToInt32(PaddingEdgeYValue.Text) }, Convert.ToInt32(SizeSquareValue.Text), colorMode);
        }

        private void PaddingEdgeBottonRight_Click(object sender, RoutedEventArgs e)
        {
            PaddingEdgeXValue.Text = Convert.ToString(Convert.ToInt32(PaddingEdgeXValue.Text) + 10);
            CreateRmatrix(Convert.ToInt32(FactorSizeValue.Text),
                new int[] { Convert.ToInt32(PaddingEdgeXValue.Text), 
                    Convert.ToInt32(PaddingEdgeYValue.Text) }, Convert.ToInt32(SizeSquareValue.Text), colorMode);
        }

        private void FactorSizeValueButtonMinus_Click(object sender, RoutedEventArgs e)
        {
            FactorSizeValue.Text = Convert.ToString(Convert.ToInt32(FactorSizeValue.Text) - 10);
            CreateRmatrix(Convert.ToInt32(FactorSizeValue.Text),
                new int[] { Convert.ToInt32(PaddingEdgeXValue.Text), 
                    Convert.ToInt32(PaddingEdgeYValue.Text) }, Convert.ToInt32(SizeSquareValue.Text), colorMode);
        }

        private void FactorSizeValueButtonPlus_Click(object sender, RoutedEventArgs e)
        {
            FactorSizeValue.Text = Convert.ToString(Convert.ToInt32(FactorSizeValue.Text) + 10);
            CreateRmatrix(Convert.ToInt32(FactorSizeValue.Text),
                new int[] { Convert.ToInt32(PaddingEdgeXValue.Text), 
                    Convert.ToInt32(PaddingEdgeYValue.Text) }, Convert.ToInt32(SizeSquareValue.Text), colorMode);
        }

        private void SizeSquareValueButtonMinus_Click(object sender, RoutedEventArgs e)
        {
            int sizeS = Convert.ToInt32(SizeSquareValue.Text) - 10 > 10 ? Convert.ToInt32(SizeSquareValue.Text) - 10 : 10;

            SizeSquareValue.Text = Convert.ToString(sizeS);
            CreateRmatrix(Convert.ToInt32(FactorSizeValue.Text),
                new int[] { Convert.ToInt32(PaddingEdgeXValue.Text), 
                    Convert.ToInt32(PaddingEdgeYValue.Text) }, Convert.ToInt32(SizeSquareValue.Text), colorMode);
        }

        private void SizeSquareValueButtonPlus_Click(object sender, RoutedEventArgs e)
        {
            SizeSquareValue.Text = Convert.ToString(Convert.ToInt32(SizeSquareValue.Text) + 10);
            CreateRmatrix(Convert.ToInt32(FactorSizeValue.Text),
                new int[] { Convert.ToInt32(PaddingEdgeXValue.Text), 
                    Convert.ToInt32(PaddingEdgeYValue.Text) }, Convert.ToInt32(SizeSquareValue.Text), colorMode);
        }

        private void LegendLine_Click(object sender, RoutedEventArgs e)
        {
            Legend.Show();
        }
        private void Universe_change_Colors(object sender, RoutedEventArgs e)
        {
            Color[] colorsLine = new Color[0];
            string[] coments = new string[0];


            MenuItem item = (MenuItem)sender;

            switch ((string)item.Header)
            {
                case "Вес ребра":
                    colorMode = 1;
                    MenuColorMode.Header = "Цветовой режим : вес ребра";
                    //найти максималное ребро
                    int maxWidth = 0;
                    for (int i = 0; i < Allegro.Elements.Count; i++)
                    {
                        for (int j = i; j < Allegro.Elements.Count; j++)
                        {
                            if (maxWidth < Allegro.Contiguity[i][j])
                                maxWidth = Allegro.Contiguity[i][j];
                        }
                    }

                    Array.Resize(ref colorsLine, maxWidth);
                    Array.Resize(ref coments, maxWidth);

                    for (int i = 0; i < maxWidth; i++)
                    {
                        colorsLine[i] = getColors(i + 1);
                        //coments[i] = Convert.ToString(i+1);
                        coments[i] = (i + 1).ToString();
                    }

                    break;
                case "Тип линии":
                    colorMode = 2;
                    MenuColorMode.Header = "Цветовой режим : тип линии";
                    Array.Resize(ref colorsLine, 3);
                    Array.Resize(ref coments, 3);
                    colorsLine[0] = getColors(0);
                    colorsLine[1] = getColors(1);
                    colorsLine[2] = getColors(2);
                    coments[0] = "прямая";
                    coments[1] = "арка";
                    coments[2] = "безье";

                    break;
                case "Дальность":
                    colorMode = 3;
                    MenuColorMode.Header = "Цветовой режим : дальность";

                    int maxDuration = 0;

                    for (int i = 0; i < Allegro.Elements.Count; i++)
                    {
                        for (int j = 0; j < Allegro.Elements.Count; j++)
                        {
                            if (maxDuration < Matrix.getDuration(i, j)
                                && Allegro.Contiguity[i][j] > 0
                                && Matrix.MatrixR[Matrix.getPositionElementFromRmatrix(i)[0]][Matrix.getPositionElementFromRmatrix(i)[1]] != -1
                                && Matrix.MatrixR[Matrix.getPositionElementFromRmatrix(j)[0]][Matrix.getPositionElementFromRmatrix(j)[1]] != -1)
                            {
                                maxDuration = Matrix.getDuration(i, j);
                                if (maxDuration == 6)
                                {

                                }
                            }
                        }
                    }

                    Array.Resize(ref colorsLine, maxDuration);
                    Array.Resize(ref coments, maxDuration);
                    for (int i = 0; i < maxDuration; i++)
                    {
                        colorsLine[i] = getColors(i);
                        coments[i] = (i + 1).ToString();
                    }


                    //newLine.Stroke = new SolidColorBrush(getColors(Matrix.getDuration(B_indexInRmatrix, A_indexInRmatrix) - 1));
                    break;
                case "Уникальный":
                    colorMode = 4;
                    MenuColorMode.Header = "Цветовой режим : Уникальный";
                    Array.Resize(ref colorsLine, 1);
                    Array.Resize(ref coments, 1);
                    colorsLine[0] = Brushes.White.Color;
                    coments[0] = "линии имеют различные оттенки";
                    break;
                default:
                    colorMode = 0;
                    MenuColorMode.Header = "Цветовой режим : монотон";
                    Array.Resize(ref colorsLine, 1);
                    Array.Resize(ref coments, 1);
                    colorsLine[0] = Brushes.Tomato.Color;
                    coments[0] = "cвязь присутствует";
                    break;
            }
            Legend.SetContent(colorsLine, coments);
            CreateRmatrix(Convert.ToInt32(FactorSizeValue.Text),
                new int[] { Convert.ToInt32(PaddingEdgeXValue.Text), 
                    Convert.ToInt32(PaddingEdgeYValue.Text) }, Convert.ToInt32(SizeSquareValue.Text), colorMode);

        }
    }
}

